﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MailerTest
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MailerTest))
        Me.grbConfig = New System.Windows.Forms.GroupBox()
        Me.splitter1 = New System.Windows.Forms.SplitContainer()
        Me.grbCreds = New System.Windows.Forms.GroupBox()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.lblPassword = New System.Windows.Forms.Label()
        Me.txtUsername = New System.Windows.Forms.TextBox()
        Me.lblUser = New System.Windows.Forms.Label()
        Me.chbCredentials = New System.Windows.Forms.CheckBox()
        Me.txtDefaultFrom = New System.Windows.Forms.TextBox()
        Me.lblDefFrom = New System.Windows.Forms.Label()
        Me.chbUseDefFrom = New System.Windows.Forms.CheckBox()
        Me.chbUseSSL = New System.Windows.Forms.CheckBox()
        Me.nudPort = New System.Windows.Forms.NumericUpDown()
        Me.lblPort = New System.Windows.Forms.Label()
        Me.txtSMTP_Server = New System.Windows.Forms.TextBox()
        Me.lblServer = New System.Windows.Forms.Label()
        Me.lblMailerState = New System.Windows.Forms.Label()
        Me.cmdUnloadMailer = New System.Windows.Forms.Button()
        Me.cmdLoadMailer = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.sbtMailerConfig = New System.Windows.Forms.ToolStripSplitButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mMailerConfigTemplate = New System.Windows.Forms.ToolStripMenuItem()
        Me.mMailerConfigINI_NoCred = New System.Windows.Forms.ToolStripMenuItem()
        Me.mMailerConfigINI_wCred = New System.Windows.Forms.ToolStripMenuItem()
        Me.grbMail = New System.Windows.Forms.GroupBox()
        Me.grbAttachments = New System.Windows.Forms.GroupBox()
        Me.cmdRemoveAttachment = New System.Windows.Forms.Button()
        Me.cmdAddAttachment = New System.Windows.Forms.Button()
        Me.lstAttachments = New System.Windows.Forms.ListBox()
        Me.txtFromAddr = New System.Windows.Forms.TextBox()
        Me.rbSendFrom = New System.Windows.Forms.RadioButton()
        Me.rbFromDefault = New System.Windows.Forms.RadioButton()
        Me.lblSendStatus = New System.Windows.Forms.Label()
        Me.cmdSendMail = New System.Windows.Forms.Button()
        Me.rtxMessage = New System.Windows.Forms.RichTextBox()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.txtSubject = New System.Windows.Forms.TextBox()
        Me.lblSubject = New System.Windows.Forms.Label()
        Me.txtBCCs = New System.Windows.Forms.TextBox()
        Me.lblBCCs = New System.Windows.Forms.Label()
        Me.txtCCs = New System.Windows.Forms.TextBox()
        Me.lblCCs = New System.Windows.Forms.Label()
        Me.txtTOs = New System.Windows.Forms.TextBox()
        Me.lblTOs = New System.Windows.Forms.Label()
        Me.cmdLoadMailObject = New System.Windows.Forms.Button()
        Me.lblMailObjState = New System.Windows.Forms.Label()
        Me.ofdINI = New System.Windows.Forms.OpenFileDialog()
        Me.ofdAttachment = New System.Windows.Forms.OpenFileDialog()
        Me.chbInsecureSSL = New System.Windows.Forms.CheckBox()
        Me.grbConfig.SuspendLayout()
        CType(Me.splitter1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splitter1.Panel1.SuspendLayout()
        Me.splitter1.Panel2.SuspendLayout()
        Me.splitter1.SuspendLayout()
        Me.grbCreds.SuspendLayout()
        CType(Me.nudPort, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.grbMail.SuspendLayout()
        Me.grbAttachments.SuspendLayout()
        Me.SuspendLayout()
        '
        'grbConfig
        '
        Me.grbConfig.Controls.Add(Me.splitter1)
        Me.grbConfig.Dock = System.Windows.Forms.DockStyle.Left
        Me.grbConfig.Location = New System.Drawing.Point(0, 0)
        Me.grbConfig.Name = "grbConfig"
        Me.grbConfig.Size = New System.Drawing.Size(229, 450)
        Me.grbConfig.TabIndex = 0
        Me.grbConfig.TabStop = False
        Me.grbConfig.Text = "Mailer Object and Config"
        '
        'splitter1
        '
        Me.splitter1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splitter1.Location = New System.Drawing.Point(3, 16)
        Me.splitter1.Name = "splitter1"
        Me.splitter1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'splitter1.Panel1
        '
        Me.splitter1.Panel1.Controls.Add(Me.chbInsecureSSL)
        Me.splitter1.Panel1.Controls.Add(Me.grbCreds)
        Me.splitter1.Panel1.Controls.Add(Me.chbCredentials)
        Me.splitter1.Panel1.Controls.Add(Me.txtDefaultFrom)
        Me.splitter1.Panel1.Controls.Add(Me.lblDefFrom)
        Me.splitter1.Panel1.Controls.Add(Me.chbUseDefFrom)
        Me.splitter1.Panel1.Controls.Add(Me.chbUseSSL)
        Me.splitter1.Panel1.Controls.Add(Me.nudPort)
        Me.splitter1.Panel1.Controls.Add(Me.lblPort)
        Me.splitter1.Panel1.Controls.Add(Me.txtSMTP_Server)
        Me.splitter1.Panel1.Controls.Add(Me.lblServer)
        '
        'splitter1.Panel2
        '
        Me.splitter1.Panel2.Controls.Add(Me.lblMailerState)
        Me.splitter1.Panel2.Controls.Add(Me.cmdUnloadMailer)
        Me.splitter1.Panel2.Controls.Add(Me.cmdLoadMailer)
        Me.splitter1.Panel2.Controls.Add(Me.StatusStrip1)
        Me.splitter1.Size = New System.Drawing.Size(223, 431)
        Me.splitter1.SplitterDistance = 332
        Me.splitter1.TabIndex = 0
        '
        'grbCreds
        '
        Me.grbCreds.Controls.Add(Me.txtPassword)
        Me.grbCreds.Controls.Add(Me.lblPassword)
        Me.grbCreds.Controls.Add(Me.txtUsername)
        Me.grbCreds.Controls.Add(Me.lblUser)
        Me.grbCreds.Location = New System.Drawing.Point(3, 227)
        Me.grbCreds.Name = "grbCreds"
        Me.grbCreds.Size = New System.Drawing.Size(200, 100)
        Me.grbCreds.TabIndex = 19
        Me.grbCreds.TabStop = False
        Me.grbCreds.Text = "SMTP Credentials"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(3, 74)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(191, 20)
        Me.txtPassword.TabIndex = 3
        Me.txtPassword.UseSystemPasswordChar = True
        '
        'lblPassword
        '
        Me.lblPassword.AutoSize = True
        Me.lblPassword.Location = New System.Drawing.Point(3, 59)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(53, 13)
        Me.lblPassword.TabIndex = 2
        Me.lblPassword.Text = "Password"
        '
        'txtUsername
        '
        Me.txtUsername.Location = New System.Drawing.Point(3, 36)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(191, 20)
        Me.txtUsername.TabIndex = 1
        '
        'lblUser
        '
        Me.lblUser.AutoSize = True
        Me.lblUser.Location = New System.Drawing.Point(3, 20)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(60, 13)
        Me.lblUser.TabIndex = 0
        Me.lblUser.Text = "User Name"
        '
        'chbCredentials
        '
        Me.chbCredentials.AutoSize = True
        Me.chbCredentials.Checked = True
        Me.chbCredentials.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chbCredentials.Location = New System.Drawing.Point(3, 204)
        Me.chbCredentials.Name = "chbCredentials"
        Me.chbCredentials.Size = New System.Drawing.Size(103, 17)
        Me.chbCredentials.TabIndex = 18
        Me.chbCredentials.Text = "With Credentials"
        Me.chbCredentials.UseVisualStyleBackColor = True
        '
        'txtDefaultFrom
        '
        Me.txtDefaultFrom.Enabled = False
        Me.txtDefaultFrom.Location = New System.Drawing.Point(3, 159)
        Me.txtDefaultFrom.Name = "txtDefaultFrom"
        Me.txtDefaultFrom.Size = New System.Drawing.Size(217, 20)
        Me.txtDefaultFrom.TabIndex = 17
        '
        'lblDefFrom
        '
        Me.lblDefFrom.AutoSize = True
        Me.lblDefFrom.Location = New System.Drawing.Point(3, 143)
        Me.lblDefFrom.Name = "lblDefFrom"
        Me.lblDefFrom.Size = New System.Drawing.Size(111, 13)
        Me.lblDefFrom.TabIndex = 16
        Me.lblDefFrom.Text = "Default From Address:"
        '
        'chbUseDefFrom
        '
        Me.chbUseDefFrom.AutoSize = True
        Me.chbUseDefFrom.Location = New System.Drawing.Point(6, 123)
        Me.chbUseDefFrom.Name = "chbUseDefFrom"
        Me.chbUseDefFrom.Size = New System.Drawing.Size(144, 17)
        Me.chbUseDefFrom.TabIndex = 15
        Me.chbUseDefFrom.Text = "Use Default From Setting"
        Me.chbUseDefFrom.UseVisualStyleBackColor = True
        '
        'chbUseSSL
        '
        Me.chbUseSSL.AutoSize = True
        Me.chbUseSSL.Location = New System.Drawing.Point(6, 85)
        Me.chbUseSSL.Name = "chbUseSSL"
        Me.chbUseSSL.Size = New System.Drawing.Size(68, 17)
        Me.chbUseSSL.TabIndex = 14
        Me.chbUseSSL.Text = "Use SSL"
        Me.chbUseSSL.UseVisualStyleBackColor = True
        '
        'nudPort
        '
        Me.nudPort.Location = New System.Drawing.Point(68, 50)
        Me.nudPort.Maximum = New Decimal(New Integer() {65535, 0, 0, 0})
        Me.nudPort.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudPort.Name = "nudPort"
        Me.nudPort.Size = New System.Drawing.Size(120, 20)
        Me.nudPort.TabIndex = 13
        Me.nudPort.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblPort
        '
        Me.lblPort.AutoSize = True
        Me.lblPort.Location = New System.Drawing.Point(3, 52)
        Me.lblPort.Name = "lblPort"
        Me.lblPort.Size = New System.Drawing.Size(59, 13)
        Me.lblPort.TabIndex = 12
        Me.lblPort.Text = "SMTP Port"
        '
        'txtSMTP_Server
        '
        Me.txtSMTP_Server.Location = New System.Drawing.Point(3, 16)
        Me.txtSMTP_Server.Name = "txtSMTP_Server"
        Me.txtSMTP_Server.Size = New System.Drawing.Size(217, 20)
        Me.txtSMTP_Server.TabIndex = 11
        '
        'lblServer
        '
        Me.lblServer.AutoSize = True
        Me.lblServer.Location = New System.Drawing.Point(3, 0)
        Me.lblServer.Name = "lblServer"
        Me.lblServer.Size = New System.Drawing.Size(71, 13)
        Me.lblServer.TabIndex = 10
        Me.lblServer.Text = "SMTP Server"
        '
        'lblMailerState
        '
        Me.lblMailerState.AutoSize = True
        Me.lblMailerState.Location = New System.Drawing.Point(9, 3)
        Me.lblMailerState.Name = "lblMailerState"
        Me.lblMailerState.Size = New System.Drawing.Size(88, 13)
        Me.lblMailerState.TabIndex = 3
        Me.lblMailerState.Tag = "Mailer not loaded"
        Me.lblMailerState.Text = "Mailer not loaded"
        '
        'cmdUnloadMailer
        '
        Me.cmdUnloadMailer.Location = New System.Drawing.Point(9, 47)
        Me.cmdUnloadMailer.Name = "cmdUnloadMailer"
        Me.cmdUnloadMailer.Size = New System.Drawing.Size(116, 23)
        Me.cmdUnloadMailer.TabIndex = 2
        Me.cmdUnloadMailer.Text = "Unload Mailer"
        Me.cmdUnloadMailer.UseVisualStyleBackColor = True
        '
        'cmdLoadMailer
        '
        Me.cmdLoadMailer.Location = New System.Drawing.Point(9, 19)
        Me.cmdLoadMailer.Name = "cmdLoadMailer"
        Me.cmdLoadMailer.Size = New System.Drawing.Size(168, 23)
        Me.cmdLoadMailer.TabIndex = 1
        Me.cmdLoadMailer.Text = "Load Mailer w. Config above"
        Me.cmdLoadMailer.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.sbtMailerConfig})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 73)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(223, 22)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 0
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'sbtMailerConfig
        '
        Me.sbtMailerConfig.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.sbtMailerConfig.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSeparator1, Me.mMailerConfigTemplate, Me.mMailerConfigINI_NoCred, Me.mMailerConfigINI_wCred})
        Me.sbtMailerConfig.Image = CType(resources.GetObject("sbtMailerConfig.Image"), System.Drawing.Image)
        Me.sbtMailerConfig.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.sbtMailerConfig.Name = "sbtMailerConfig"
        Me.sbtMailerConfig.Size = New System.Drawing.Size(95, 20)
        Me.sbtMailerConfig.Text = "Mailer Config"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(291, 6)
        '
        'mMailerConfigTemplate
        '
        Me.mMailerConfigTemplate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.mMailerConfigTemplate.Name = "mMailerConfigTemplate"
        Me.mMailerConfigTemplate.Size = New System.Drawing.Size(294, 22)
        Me.mMailerConfigTemplate.Text = "Use Result of Function (Testapp Template)"
        '
        'mMailerConfigINI_NoCred
        '
        Me.mMailerConfigINI_NoCred.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.mMailerConfigINI_NoCred.Name = "mMailerConfigINI_NoCred"
        Me.mMailerConfigINI_NoCred.Size = New System.Drawing.Size(294, 22)
        Me.mMailerConfigINI_NoCred.Text = "Load using INI File (no Cred.)"
        '
        'mMailerConfigINI_wCred
        '
        Me.mMailerConfigINI_wCred.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.mMailerConfigINI_wCred.Name = "mMailerConfigINI_wCred"
        Me.mMailerConfigINI_wCred.Size = New System.Drawing.Size(294, 22)
        Me.mMailerConfigINI_wCred.Text = "Load using INI File (w Cred.)"
        '
        'grbMail
        '
        Me.grbMail.Controls.Add(Me.grbAttachments)
        Me.grbMail.Controls.Add(Me.txtFromAddr)
        Me.grbMail.Controls.Add(Me.rbSendFrom)
        Me.grbMail.Controls.Add(Me.rbFromDefault)
        Me.grbMail.Controls.Add(Me.lblSendStatus)
        Me.grbMail.Controls.Add(Me.cmdSendMail)
        Me.grbMail.Controls.Add(Me.rtxMessage)
        Me.grbMail.Controls.Add(Me.lblMessage)
        Me.grbMail.Controls.Add(Me.txtSubject)
        Me.grbMail.Controls.Add(Me.lblSubject)
        Me.grbMail.Controls.Add(Me.txtBCCs)
        Me.grbMail.Controls.Add(Me.lblBCCs)
        Me.grbMail.Controls.Add(Me.txtCCs)
        Me.grbMail.Controls.Add(Me.lblCCs)
        Me.grbMail.Controls.Add(Me.txtTOs)
        Me.grbMail.Controls.Add(Me.lblTOs)
        Me.grbMail.Controls.Add(Me.cmdLoadMailObject)
        Me.grbMail.Controls.Add(Me.lblMailObjState)
        Me.grbMail.Dock = System.Windows.Forms.DockStyle.Right
        Me.grbMail.Location = New System.Drawing.Point(235, 0)
        Me.grbMail.Name = "grbMail"
        Me.grbMail.Size = New System.Drawing.Size(753, 450)
        Me.grbMail.TabIndex = 1
        Me.grbMail.TabStop = False
        Me.grbMail.Text = "Mail"
        '
        'grbAttachments
        '
        Me.grbAttachments.Controls.Add(Me.cmdRemoveAttachment)
        Me.grbAttachments.Controls.Add(Me.cmdAddAttachment)
        Me.grbAttachments.Controls.Add(Me.lstAttachments)
        Me.grbAttachments.Dock = System.Windows.Forms.DockStyle.Right
        Me.grbAttachments.Location = New System.Drawing.Point(565, 16)
        Me.grbAttachments.Name = "grbAttachments"
        Me.grbAttachments.Size = New System.Drawing.Size(185, 431)
        Me.grbAttachments.TabIndex = 13
        Me.grbAttachments.TabStop = False
        Me.grbAttachments.Text = "Attachments"
        '
        'cmdRemoveAttachment
        '
        Me.cmdRemoveAttachment.Location = New System.Drawing.Point(51, 19)
        Me.cmdRemoveAttachment.Name = "cmdRemoveAttachment"
        Me.cmdRemoveAttachment.Size = New System.Drawing.Size(39, 23)
        Me.cmdRemoveAttachment.TabIndex = 1
        Me.cmdRemoveAttachment.Text = "-"
        Me.cmdRemoveAttachment.UseVisualStyleBackColor = True
        '
        'cmdAddAttachment
        '
        Me.cmdAddAttachment.Location = New System.Drawing.Point(6, 19)
        Me.cmdAddAttachment.Name = "cmdAddAttachment"
        Me.cmdAddAttachment.Size = New System.Drawing.Size(39, 23)
        Me.cmdAddAttachment.TabIndex = 1
        Me.cmdAddAttachment.Text = "+"
        Me.cmdAddAttachment.UseVisualStyleBackColor = True
        '
        'lstAttachments
        '
        Me.lstAttachments.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lstAttachments.FormattingEnabled = True
        Me.lstAttachments.Location = New System.Drawing.Point(3, 47)
        Me.lstAttachments.Name = "lstAttachments"
        Me.lstAttachments.Size = New System.Drawing.Size(179, 381)
        Me.lstAttachments.TabIndex = 0
        '
        'txtFromAddr
        '
        Me.txtFromAddr.Enabled = False
        Me.txtFromAddr.Location = New System.Drawing.Point(414, 31)
        Me.txtFromAddr.Name = "txtFromAddr"
        Me.txtFromAddr.Size = New System.Drawing.Size(145, 20)
        Me.txtFromAddr.TabIndex = 12
        '
        'rbSendFrom
        '
        Me.rbSendFrom.AutoSize = True
        Me.rbSendFrom.Location = New System.Drawing.Point(248, 32)
        Me.rbSendFrom.Name = "rbSendFrom"
        Me.rbSendFrom.Size = New System.Drawing.Size(160, 17)
        Me.rbSendFrom.TabIndex = 11
        Me.rbSendFrom.TabStop = True
        Me.rbSendFrom.Text = "Send from following address:"
        Me.rbSendFrom.UseVisualStyleBackColor = True
        '
        'rbFromDefault
        '
        Me.rbFromDefault.AutoSize = True
        Me.rbFromDefault.Checked = True
        Me.rbFromDefault.Location = New System.Drawing.Point(248, 14)
        Me.rbFromDefault.Name = "rbFromDefault"
        Me.rbFromDefault.Size = New System.Drawing.Size(148, 17)
        Me.rbFromDefault.TabIndex = 10
        Me.rbFromDefault.TabStop = True
        Me.rbFromDefault.Text = "Send from default address"
        Me.rbFromDefault.UseVisualStyleBackColor = True
        '
        'lblSendStatus
        '
        Me.lblSendStatus.AutoSize = True
        Me.lblSendStatus.Location = New System.Drawing.Point(98, 430)
        Me.lblSendStatus.Name = "lblSendStatus"
        Me.lblSendStatus.Size = New System.Drawing.Size(101, 13)
        Me.lblSendStatus.TabIndex = 9
        Me.lblSendStatus.Text = "No mail was sent jet"
        '
        'cmdSendMail
        '
        Me.cmdSendMail.Location = New System.Drawing.Point(6, 425)
        Me.cmdSendMail.Name = "cmdSendMail"
        Me.cmdSendMail.Size = New System.Drawing.Size(75, 23)
        Me.cmdSendMail.TabIndex = 8
        Me.cmdSendMail.Text = "Send"
        Me.cmdSendMail.UseVisualStyleBackColor = True
        '
        'rtxMessage
        '
        Me.rtxMessage.Location = New System.Drawing.Point(6, 263)
        Me.rtxMessage.Name = "rtxMessage"
        Me.rtxMessage.Size = New System.Drawing.Size(544, 159)
        Me.rtxMessage.TabIndex = 7
        Me.rtxMessage.Text = ""
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.Location = New System.Drawing.Point(6, 251)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(53, 13)
        Me.lblMessage.TabIndex = 6
        Me.lblMessage.Text = "Message:"
        '
        'txtSubject
        '
        Me.txtSubject.Location = New System.Drawing.Point(6, 228)
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.Size = New System.Drawing.Size(544, 20)
        Me.txtSubject.TabIndex = 5
        '
        'lblSubject
        '
        Me.lblSubject.AutoSize = True
        Me.lblSubject.Location = New System.Drawing.Point(6, 212)
        Me.lblSubject.Name = "lblSubject"
        Me.lblSubject.Size = New System.Drawing.Size(46, 13)
        Me.lblSubject.TabIndex = 4
        Me.lblSubject.Text = "Subject:"
        '
        'txtBCCs
        '
        Me.txtBCCs.Location = New System.Drawing.Point(6, 175)
        Me.txtBCCs.Multiline = True
        Me.txtBCCs.Name = "txtBCCs"
        Me.txtBCCs.Size = New System.Drawing.Size(544, 34)
        Me.txtBCCs.TabIndex = 3
        '
        'lblBCCs
        '
        Me.lblBCCs.AutoSize = True
        Me.lblBCCs.Location = New System.Drawing.Point(3, 159)
        Me.lblBCCs.Name = "lblBCCs"
        Me.lblBCCs.Size = New System.Drawing.Size(83, 13)
        Me.lblBCCs.TabIndex = 2
        Me.lblBCCs.Text = "BCC Addresses:"
        '
        'txtCCs
        '
        Me.txtCCs.Location = New System.Drawing.Point(6, 122)
        Me.txtCCs.Multiline = True
        Me.txtCCs.Name = "txtCCs"
        Me.txtCCs.Size = New System.Drawing.Size(544, 34)
        Me.txtCCs.TabIndex = 3
        '
        'lblCCs
        '
        Me.lblCCs.AutoSize = True
        Me.lblCCs.Location = New System.Drawing.Point(3, 106)
        Me.lblCCs.Name = "lblCCs"
        Me.lblCCs.Size = New System.Drawing.Size(76, 13)
        Me.lblCCs.TabIndex = 2
        Me.lblCCs.Text = "CC Addresses:"
        '
        'txtTOs
        '
        Me.txtTOs.Location = New System.Drawing.Point(9, 68)
        Me.txtTOs.Multiline = True
        Me.txtTOs.Name = "txtTOs"
        Me.txtTOs.Size = New System.Drawing.Size(544, 34)
        Me.txtTOs.TabIndex = 3
        '
        'lblTOs
        '
        Me.lblTOs.AutoSize = True
        Me.lblTOs.Location = New System.Drawing.Point(6, 52)
        Me.lblTOs.Name = "lblTOs"
        Me.lblTOs.Size = New System.Drawing.Size(75, 13)
        Me.lblTOs.TabIndex = 2
        Me.lblTOs.Text = "To Addresses:"
        '
        'cmdLoadMailObject
        '
        Me.cmdLoadMailObject.Location = New System.Drawing.Point(121, 10)
        Me.cmdLoadMailObject.Name = "cmdLoadMailObject"
        Me.cmdLoadMailObject.Size = New System.Drawing.Size(99, 24)
        Me.cmdLoadMailObject.TabIndex = 1
        Me.cmdLoadMailObject.Text = "Load Mail Object"
        Me.cmdLoadMailObject.UseVisualStyleBackColor = True
        '
        'lblMailObjState
        '
        Me.lblMailObjState.AutoSize = True
        Me.lblMailObjState.Location = New System.Drawing.Point(6, 16)
        Me.lblMailObjState.Name = "lblMailObjState"
        Me.lblMailObjState.Size = New System.Drawing.Size(109, 13)
        Me.lblMailObjState.TabIndex = 0
        Me.lblMailObjState.Tag = "No mail object loaded"
        Me.lblMailObjState.Text = "No mail object loaded"
        '
        'ofdINI
        '
        Me.ofdINI.FileName = "MailerConfig"
        Me.ofdINI.Filter = "Ini-Files|*.ini;*.inf;*.conf"
        Me.ofdINI.Title = "Select MailerConfig containing INI file"
        '
        'ofdAttachment
        '
        Me.ofdAttachment.FileName = "any attachment file"
        Me.ofdAttachment.Filter = "All files|*.*"
        Me.ofdAttachment.Multiselect = True
        '
        'chbInsecureSSL
        '
        Me.chbInsecureSSL.AutoSize = True
        Me.chbInsecureSSL.Location = New System.Drawing.Point(80, 85)
        Me.chbInsecureSSL.Name = "chbInsecureSSL"
        Me.chbInsecureSSL.Size = New System.Drawing.Size(97, 17)
        Me.chbInsecureSSL.TabIndex = 20
        Me.chbInsecureSSL.Text = "Insecure Certs."
        Me.chbInsecureSSL.UseVisualStyleBackColor = True
        '
        'MailerTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(988, 450)
        Me.Controls.Add(Me.grbMail)
        Me.Controls.Add(Me.grbConfig)
        Me.Name = "MailerTest"
        Me.Text = "Mailer Test"
        Me.grbConfig.ResumeLayout(False)
        Me.splitter1.Panel1.ResumeLayout(False)
        Me.splitter1.Panel1.PerformLayout()
        Me.splitter1.Panel2.ResumeLayout(False)
        Me.splitter1.Panel2.PerformLayout()
        CType(Me.splitter1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splitter1.ResumeLayout(False)
        Me.grbCreds.ResumeLayout(False)
        Me.grbCreds.PerformLayout()
        CType(Me.nudPort, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.grbMail.ResumeLayout(False)
        Me.grbMail.PerformLayout()
        Me.grbAttachments.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents grbConfig As GroupBox
    Friend WithEvents splitter1 As SplitContainer
    Friend WithEvents grbCreds As GroupBox
    Friend WithEvents txtPassword As TextBox
    Friend WithEvents lblPassword As Label
    Friend WithEvents txtUsername As TextBox
    Friend WithEvents lblUser As Label
    Friend WithEvents chbCredentials As CheckBox
    Friend WithEvents txtDefaultFrom As TextBox
    Friend WithEvents lblDefFrom As Label
    Friend WithEvents chbUseDefFrom As CheckBox
    Friend WithEvents chbUseSSL As CheckBox
    Friend WithEvents nudPort As NumericUpDown
    Friend WithEvents lblPort As Label
    Friend WithEvents txtSMTP_Server As TextBox
    Friend WithEvents lblServer As Label
    Friend WithEvents lblMailerState As Label
    Friend WithEvents cmdUnloadMailer As Button
    Friend WithEvents cmdLoadMailer As Button
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents sbtMailerConfig As ToolStripSplitButton
    Friend WithEvents mMailerConfigTemplate As ToolStripMenuItem
    Friend WithEvents mMailerConfigINI_NoCred As ToolStripMenuItem
    Friend WithEvents grbMail As GroupBox
    Friend WithEvents cmdLoadMailObject As Button
    Friend WithEvents lblMailObjState As Label
    Friend WithEvents lblSendStatus As Label
    Friend WithEvents cmdSendMail As Button
    Friend WithEvents rtxMessage As RichTextBox
    Friend WithEvents lblMessage As Label
    Friend WithEvents txtSubject As TextBox
    Friend WithEvents lblSubject As Label
    Friend WithEvents txtBCCs As TextBox
    Friend WithEvents lblBCCs As Label
    Friend WithEvents txtCCs As TextBox
    Friend WithEvents lblCCs As Label
    Friend WithEvents txtTOs As TextBox
    Friend WithEvents lblTOs As Label
    Friend WithEvents ofdINI As OpenFileDialog
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents mMailerConfigINI_wCred As ToolStripMenuItem
    Friend WithEvents txtFromAddr As TextBox
    Friend WithEvents rbSendFrom As RadioButton
    Friend WithEvents rbFromDefault As RadioButton
    Friend WithEvents grbAttachments As GroupBox
    Friend WithEvents cmdRemoveAttachment As Button
    Friend WithEvents cmdAddAttachment As Button
    Friend WithEvents lstAttachments As ListBox
    Friend WithEvents ofdAttachment As OpenFileDialog
    Friend WithEvents chbInsecureSSL As CheckBox
End Class
