﻿Imports pmNETutils.Mailing

' This is a Template that has a function that you must implement. Rewrite the function below which must create a MailerConfig object.
' The mailer test implements a option to call the function to get a MailerConfig(_wCred) obj.
' Then this object will be used to create a new mailer object. 
' The intension of this template is to demonstrate the use of MailerConfig with loaded values from other ways as provided
' (like loaded data into a new class object by a JSON decoder, XML decoder, etc.).

Public Module ConfigTeamplate
    Public Function UseMailerConfig() As MailerConfig
        Throw New NotImplementedException
    End Function
End Module