﻿Imports pmNETutils.Mailing
Imports pmNETutils.Configurations.INI
Public Class MailerTest
    Private m As Mailer = Nothing
    Private mail As PreparedMail = Nothing
    Private attachs As New Dictionary(Of String, String)

    Private Property MailerObj As Mailer
        Get
            Return m
        End Get
        Set(value As Mailer)
            m = value
            If value Is Nothing Then
                grbMail.Enabled = False
                mail = Nothing
                lblMailerState.Text = CStr(lblMailerState.Tag)
            Else
                grbMail.Enabled = True
                lblMailerState.Text = "Mailer object loaded"
            End If
        End Set
    End Property

    Private Sub MailerTest_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        TestSelectorForm.Show()
    End Sub

    Private Sub chbUseDefFrom_CheckedChanged(sender As Object, e As EventArgs) Handles chbUseDefFrom.CheckedChanged
        txtDefaultFrom.Enabled = chbUseDefFrom.Checked
    End Sub

    Private Sub chbCredentials_CheckedChanged(sender As Object, e As EventArgs) Handles chbCredentials.CheckedChanged
        grbCreds.Enabled = chbCredentials.Checked
    End Sub

    Private Sub cmdUnloadMailer_Click(sender As Object, e As EventArgs) Handles cmdUnloadMailer.Click
        MailerObj = Nothing
    End Sub

    Private Sub cmdLoadMailer_Click(sender As Object, e As EventArgs) Handles cmdLoadMailer.Click
        If chbCredentials.Checked Then
            MailerObj = New Mailer(txtSMTP_Server.Text, nudPort.Value, txtUsername.Text, txtPassword.Text, chbUseSSL.Checked, IIf(chbUseDefFrom.Checked, txtDefaultFrom.Text, Nothing))
        Else
            MailerObj = New Mailer(txtSMTP_Server.Text, CInt(nudPort.Value), chbUseSSL.Checked, IIf(chbUseDefFrom.Checked, txtDefaultFrom.Text, Nothing))
        End If
    End Sub

    Private Sub mMailerConfigTemplate_Click(sender As Object, e As EventArgs) Handles mMailerConfigTemplate.Click
        Dim conf As MailerConfig
        Try
            conf = UseMailerConfig()
            Config2Gui(conf)
            MailerObj = New Mailer(conf)
        Catch nImpl As NotImplementedException
            MsgBox("You can not use this function in this example, because you must reimplement it as shown in Code file.", MsgBoxStyle.Exclamation, "Not possible")
        End Try
    End Sub

    Private Sub mMailerConfigINI_NoCred_Click(sender As Object, e As EventArgs) Handles mMailerConfigINI_NoCred.Click
        LoadIniConfig(False)
    End Sub
    Private Sub mMailerConfigINI_wCred_Click(sender As Object, e As EventArgs) Handles mMailerConfigINI_wCred.Click
        LoadIniConfig(True)
    End Sub

    Private Sub LoadIniConfig(wCred As Boolean)
        Dim conf As MailerConfig
        Dim secname As String
        Dim file As INI_File
        Dim sec As INI_Section ' you do not need to use a extra object. used here to tell key match. use file.section(secname) and throw it into the config loader instead of sec
        If ofdINI.ShowDialog() = DialogResult.OK Then
            secname = InputBox("Write the section to use to load from the INI file.", "MailerConf from INI file", "MailerConfig")
            If secname.Length = 0 Then
                MsgBox("You must specify a section name!", MsgBoxStyle.Exclamation, "MailerConf from INI file")
            Else
                file = INI_File.OpenINIfile(ofdINI.FileName)
                If file Is Nothing Then Throw New Exception("Error, INI file object is nothing")
                If Not file.HasSection(secname) Then
                    MsgBox("The section name '" & secname & "' was not found in file!", MsgBoxStyle.Critical, "MailerConfig: Section not found in INI file")
                    Exit Sub
                End If
                sec = file.GetSection(secname)
                ' key match check for test app
                Debug.Print("key 'SMTP_Server' " & IIf(sec.HasKey("SMTP_Server"), "found", "not found"))
                Debug.Print("key 'SMTP_Port' " & IIf(sec.HasKey("SMTP_Port"), "found", "not found"))
                Debug.Print("key 'UseSSL' " & IIf(sec.HasKey("UseSSL"), "found", "not found"))
                Debug.Print("key 'InsecureSSL' " & IIf(sec.HasKey("InsecureSSL"), "found", "not found"))
                Debug.Print("key 'DefFrom' " & IIf(sec.HasKey("DefFrom"), "found", "not found"))
                If wCred Then
                    Debug.Print("key 'LogUser' " & IIf(sec.HasKey("LogUser"), "found", "not found"))
                    Debug.Print("key 'LogPass' " & IIf(sec.HasKey("LogPass"), "found", "not found"))
                    conf = MailerConfig_wCred.FromINISection(sec)
                Else
                    Debug.Print("No Credentials loaded")
                    conf = MailerConfig.FromINISection(sec)
                End If
                Config2Gui(conf)
                MailerObj = New Mailer(conf)
            End If
        End If
    End Sub

    Private Sub Config2Gui(conf As MailerConfig)
        Dim conf_wC As MailerConfig_wCred
        txtSMTP_Server.Text = conf.SMTP_Server
        nudPort.Value = conf.SMTP_Port
        chbUseSSL.Checked = conf.UseSSL
        chbInsecureSSL.Checked = conf.InsecureSSL
        If conf.DefFrom IsNot Nothing AndAlso conf.DefFrom.Length > 0 Then
            chbUseDefFrom.Checked = True
            txtDefaultFrom.Text = conf.DefFrom
        Else
            chbUseDefFrom.Checked = False
            txtDefaultFrom.Text = ""
        End If
        If conf.ConfigID = 1 Then
            conf_wC = CType(conf, MailerConfig_wCred)
            chbCredentials.Checked = True
            txtUsername.Text = conf_wC.LogUser
            txtPassword.Text = conf_wC.LogPass
        End If
    End Sub

    Private Sub cmdLoadMailObject_Click(sender As Object, e As EventArgs) Handles cmdLoadMailObject.Click
        mail = MailerObj.PrepareMail()
        lblMailObjState.Text = "Mail object is loaded"
    End Sub

    Private Sub rbSendFrom_CheckedChanged(sender As Object, e As EventArgs) Handles rbSendFrom.CheckedChanged
        txtFromAddr.Enabled = rbSendFrom.Checked
    End Sub

    Private Sub cmdSendMail_Click(sender As Object, e As EventArgs) Handles cmdSendMail.Click
        If attachs.Count > 0 Then mail.AddAttachments(attachs.Values.ToList)
        ' just for testing test 3 possible address combi's
        If rbFromDefault.Checked Then
            If txtTOs.Text.Length > 0 And txtCCs.Text.Length > 0 And txtBCCs.Text.Length > 0 Then
                mail.PrepareHead(txtTOs.Text, txtCCs.Text, txtBCCs.Text)
            ElseIf txtTOs.Text.Length > 0 And txtCCs.Text.Length > 0 Then
                mail.PrepareHead(txtTOs.Text, txtCCs.Text)
            ElseIf txtTOs.Text.Length > 0 Then
                mail.PrepareHead(txtTOs.Text)
            End If
        Else
            If txtTOs.Text.Length > 0 And txtCCs.Text.Length > 0 And txtBCCs.Text.Length > 0 Then
                mail.PrepareHead(txtTOs.Text, txtCCs.Text, txtBCCs.Text, txtFromAddr.Text)
            ElseIf txtTOs.Text.Length > 0 And txtCCs.Text.Length > 0 Then
                mail.PrepareHead(txtTOs.Text, txtCCs.Text, txtFromAddr.Text)
            ElseIf txtTOs.Text.Length > 0 Then
                mail.PrepareHead(txtTOs.Text, txtFromAddr.Text)
            End If
        End If
        mail.Subject = txtSubject.Text
        mail.MailBody = rtxMessage.Text
        mail.Send()
        lblSendStatus.Text = "Mail was sent at " & Now.ToString
        lstAttachments.Items.Clear()
        attachs.Clear()
        mail = Nothing
        lblMailObjState.Text = CStr(lblMailObjState.Tag)
    End Sub

    Private Sub cmdAddAttachment_Click(sender As Object, e As EventArgs) Handles cmdAddAttachment.Click
        Dim short_fn As String
        If ofdAttachment.ShowDialog = DialogResult.OK Then
            For Each fpath As String In ofdAttachment.FileNames
                short_fn = fpath.Substring(fpath.LastIndexOf("\") + 1)
                lstAttachments.Items.Add(short_fn)
                attachs.Add(short_fn, fpath)
            Next
        End If
    End Sub

    Private Sub cmdRemoveAttachment_Click(sender As Object, e As EventArgs) Handles cmdRemoveAttachment.Click
        If lstAttachments.Items.Count = 0 Then Exit Sub
        attachs.Remove(lstAttachments.SelectedItem)
        lstAttachments.Items.Remove(lstAttachments.SelectedItem)
    End Sub
End Class