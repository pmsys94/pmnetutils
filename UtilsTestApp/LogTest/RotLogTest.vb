﻿Imports pmNETutils.Logging
Public Class RotLogTest
    Inherits SimpleLogTest

    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents StopAutoRotate As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents StartAutoRot_At As ToolStripMenuItem
    Friend WithEvents StartAutoRot_Time As ToolStripMenuItem
    Friend WithEvents dbtAutoRotate As ToolStripDropDownButton

    Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub

    Friend Overrides Sub LoadObject()
        LogObj = RotatedLogfile.Create(3)
    End Sub

    Private Sub StopAutoRotate_Click(sender As Object, e As EventArgs) Handles StopAutoRotate.Click
        If LogObj IsNot Nothing Then
            Dim rotlog As RotatedLogfile = LogObj
            rotlog.StopAutoRotation()
        Else
            MsgBox("Logfile object is Nothing!", MsgBoxStyle.Critical, "Rot. Log is nothing")
        End If
    End Sub

    Private Sub StartAutoRot_At_Click(sender As Object, e As EventArgs) Handles StartAutoRot_At.Click
        If LogObj IsNot Nothing Then
            Dim rotlog As RotatedLogfile = LogObj
            rotlog.StartRotationAt(AutoRotationAt.BeginOfDay)
        Else
            MsgBox("Logfile object is Nothing!", MsgBoxStyle.Critical, "Rot. Log is nothing")
        End If
    End Sub

    Private Sub StartAutoRot_Time_Click(sender As Object, e As EventArgs) Handles StartAutoRot_Time.Click
        If LogObj IsNot Nothing Then
            Dim rotlog As RotatedLogfile = LogObj
            rotlog.StartAutoRotation(5)
        Else
            MsgBox("Logfile object is Nothing!", MsgBoxStyle.Critical, "Rot. Log is nothing")
        End If
    End Sub

    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RotLogTest))
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.dbtAutoRotate = New System.Windows.Forms.ToolStripDropDownButton()
        Me.StopAutoRotate = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.StartAutoRot_At = New System.Windows.Forms.ToolStripMenuItem()
        Me.StartAutoRot_Time = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.dbtAutoRotate})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 146)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(348, 22)
        Me.StatusStrip1.TabIndex = 11
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'dbtAutoRotate
        '
        Me.dbtAutoRotate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.dbtAutoRotate.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StopAutoRotate, Me.ToolStripSeparator1, Me.StartAutoRot_At, Me.StartAutoRot_Time})
        Me.dbtAutoRotate.Image = CType(resources.GetObject("dbtAutoRotate.Image"), System.Drawing.Image)
        Me.dbtAutoRotate.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.dbtAutoRotate.Name = "dbtAutoRotate"
        Me.dbtAutoRotate.Size = New System.Drawing.Size(83, 20)
        Me.dbtAutoRotate.Text = "Auto Rotate"
        '
        'StopAutoRotate
        '
        Me.StopAutoRotate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.StopAutoRotate.Name = "StopAutoRotate"
        Me.StopAutoRotate.Size = New System.Drawing.Size(180, 22)
        Me.StopAutoRotate.Text = "Stop"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(177, 6)
        '
        'StartAutoRot_At
        '
        Me.StartAutoRot_At.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.StartAutoRot_At.Name = "StartAutoRot_At"
        Me.StartAutoRot_At.Size = New System.Drawing.Size(180, 22)
        Me.StartAutoRot_At.Text = "Start At"
        '
        'StartAutoRot_Time
        '
        Me.StartAutoRot_Time.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.StartAutoRot_Time.Name = "StartAutoRot_Time"
        Me.StartAutoRot_Time.Size = New System.Drawing.Size(180, 22)
        Me.StartAutoRot_Time.Text = "Start Interval"
        '
        'RotLogTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(348, 168)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "RotLogTest"
        Me.Text = "Rotated Logfile Test"
        Me.Controls.SetChildIndex(Me.StatusStrip1, 0)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
End Class
