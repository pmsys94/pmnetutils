﻿Public Class LogTestSelect
    Private Sub cmdContinue_Click(sender As Object, e As EventArgs) Handles cmdContinue.Click
        Select Case cboLogType.SelectedIndex
            Case 0
                SimpleLogTest.Show()
            Case 1
                DateLogTest.Show()
            Case 2
                RotLogTest.Show()
        End Select
        Close()
    End Sub

    Private Sub LogTestSelect_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cboLogType.SelectedIndex = 0
    End Sub
End Class