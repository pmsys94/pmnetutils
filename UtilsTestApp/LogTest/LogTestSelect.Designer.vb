﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LogTestSelect
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cboLogType = New System.Windows.Forms.ComboBox()
        Me.lblLogTypeSelect = New System.Windows.Forms.Label()
        Me.cmdContinue = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cboLogType
        '
        Me.cboLogType.FormattingEnabled = True
        Me.cboLogType.Items.AddRange(New Object() {"Simple Logfile", "Date Logfile", "Rotated Logfile"})
        Me.cboLogType.Location = New System.Drawing.Point(12, 39)
        Me.cboLogType.Name = "cboLogType"
        Me.cboLogType.Size = New System.Drawing.Size(121, 21)
        Me.cboLogType.TabIndex = 0
        '
        'lblLogTypeSelect
        '
        Me.lblLogTypeSelect.AutoSize = True
        Me.lblLogTypeSelect.Location = New System.Drawing.Point(20, 20)
        Me.lblLogTypeSelect.Name = "lblLogTypeSelect"
        Me.lblLogTypeSelect.Size = New System.Drawing.Size(98, 13)
        Me.lblLogTypeSelect.TabIndex = 1
        Me.lblLogTypeSelect.Text = "Select Logfile Type"
        '
        'cmdContinue
        '
        Me.cmdContinue.Location = New System.Drawing.Point(23, 66)
        Me.cmdContinue.Name = "cmdContinue"
        Me.cmdContinue.Size = New System.Drawing.Size(75, 23)
        Me.cmdContinue.TabIndex = 2
        Me.cmdContinue.Text = "Continue"
        Me.cmdContinue.UseVisualStyleBackColor = True
        '
        'LogTestSelect
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(165, 93)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdContinue)
        Me.Controls.Add(Me.lblLogTypeSelect)
        Me.Controls.Add(Me.cboLogType)
        Me.Name = "LogTestSelect"
        Me.Text = "Test Logfiles"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cboLogType As ComboBox
    Friend WithEvents lblLogTypeSelect As Label
    Friend WithEvents cmdContinue As Button
End Class
