﻿Imports pmNETutils.Logging
Public Class SimpleLogTest

    Friend LogObj As Logfile

    Private Sub cmdLoadObject_Click(sender As Object, e As EventArgs) Handles cmdLoadObject.Click
        LoadObject()
        lblObjectState.Text = "Logfile object loaded"
    End Sub

    Private Sub cmdRemObject_Click(sender As Object, e As EventArgs) Handles cmdRemObject.Click
        LogObj = Nothing
        lblObjectState.Text = "Logfile object unloaded"
    End Sub

    Friend Overridable Sub LoadObject()
        LogObj = New Logfile
    End Sub

    Private Sub cmdSend2Log_Click(sender As Object, e As EventArgs) Handles cmdSend2Log.Click
        If LogObj IsNot Nothing Then
            If rbNormalMsg.Checked Then
                LogObj.NormalLine(txtMsg.Text)
            ElseIf rbWarningMsg.Checked Then
                LogObj.WarningLine(txtMsg.Text)
            ElseIf rbErrorMsg.Checked Then
                LogObj.ErrorLine(txtMsg.Text)
            End If
            lblLogSent.Text = CStr(lblLogSent.Tag) & Now.ToLongTimeString()
        Else
            MsgBox("Logfile object is not loaded!", MsgBoxStyle.Critical, "Logfile not loaded")
        End If
    End Sub

    Private Sub SimpleLogTest_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        TestSelectorForm.Show()
    End Sub

    Private Sub txtMsg_KeyDown(sender As Object, e As KeyEventArgs) Handles txtMsg.KeyDown
        If e.KeyCode = Keys.Enter Then cmdSend2Log.PerformClick()
    End Sub

    Private Sub cmdSetupMailer_Click(sender As Object, e As EventArgs) Handles cmdSetupMailer.Click
        Dim configSecname As String
        Dim fa As String = ""
        Dim ta As String() = Nothing
        Dim ini As pmNETutils.Configurations.INI.INI_File
        If ofdErrMailerConfig.ShowDialog() = DialogResult.OK Then
            configSecname = InputBox("Tell the name of the section of the mailer config keys.", "Err Mailer config", "MailerConfig")
            ini = pmNETutils.Configurations.INI.INI_File.OpenINIfile(ofdErrMailerConfig.FileName)
            If Not ini.HasSection(configSecname) Then
                MsgBox("Secname '" & configSecname & "' not found in INI file", MsgBoxStyle.Exclamation, "Error mailer config - section not found")
                Exit Sub
            End If
            If MsgBox("Do you want to have a FROM address (not use default from)?", MsgBoxStyle.YesNo, "Use your extra from address?") = MsgBoxResult.Yes Then
                fa = InputBox("Tell the FROM address.", "Error Mailer From address")
            End If
            If MsgBox("Do you want to tell the TO address extra?", MsgBoxStyle.YesNo, "Use extra to address") = MsgBoxResult.Yes Then
                ta = {InputBox("Tell the to address.", "Error Mailer To address")}
                If ta(0).Length = 0 Then ta = Nothing
            End If
            If MsgBox("Use Mailer with Credentials?", MsgBoxStyle.YesNo, "Loading error mailer") = MsgBoxResult.Yes Then
                LogObj.SetupMailer(ErrMailerConfig.FromINISection(ini.GetSection(configSecname), True), fa, ta)
            Else
                LogObj.SetupMailer(ErrMailerConfig.FromINISection(ini.GetSection(configSecname), False), fa, ta)
            End If
        End If
    End Sub

    Private Sub cmdStopMailer_Click(sender As Object, e As EventArgs) Handles cmdStopMailer.Click
        LogObj.StopMailer(True)
    End Sub
End Class