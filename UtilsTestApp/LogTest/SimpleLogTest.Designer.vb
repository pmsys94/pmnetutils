﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SimpleLogTest
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblObjectState = New System.Windows.Forms.Label()
        Me.cmdLoadObject = New System.Windows.Forms.Button()
        Me.cmdRemObject = New System.Windows.Forms.Button()
        Me.rbNormalMsg = New System.Windows.Forms.RadioButton()
        Me.rbWarningMsg = New System.Windows.Forms.RadioButton()
        Me.rbErrorMsg = New System.Windows.Forms.RadioButton()
        Me.txtMsg = New System.Windows.Forms.TextBox()
        Me.cmdSend2Log = New System.Windows.Forms.Button()
        Me.cmdSetupMailer = New System.Windows.Forms.Button()
        Me.cmdStopMailer = New System.Windows.Forms.Button()
        Me.lblLogSent = New System.Windows.Forms.Label()
        Me.ofdErrMailerConfig = New System.Windows.Forms.OpenFileDialog()
        Me.SuspendLayout()
        '
        'lblObjectState
        '
        Me.lblObjectState.AutoSize = True
        Me.lblObjectState.Location = New System.Drawing.Point(12, 9)
        Me.lblObjectState.Name = "lblObjectState"
        Me.lblObjectState.Size = New System.Drawing.Size(88, 13)
        Me.lblObjectState.TabIndex = 0
        Me.lblObjectState.Text = "No object loaded"
        '
        'cmdLoadObject
        '
        Me.cmdLoadObject.Location = New System.Drawing.Point(12, 35)
        Me.cmdLoadObject.Name = "cmdLoadObject"
        Me.cmdLoadObject.Size = New System.Drawing.Size(75, 23)
        Me.cmdLoadObject.TabIndex = 1
        Me.cmdLoadObject.Text = "Load Object"
        Me.cmdLoadObject.UseVisualStyleBackColor = True
        '
        'cmdRemObject
        '
        Me.cmdRemObject.Location = New System.Drawing.Point(93, 35)
        Me.cmdRemObject.Name = "cmdRemObject"
        Me.cmdRemObject.Size = New System.Drawing.Size(93, 23)
        Me.cmdRemObject.TabIndex = 2
        Me.cmdRemObject.Text = "Remove Object"
        Me.cmdRemObject.UseVisualStyleBackColor = True
        '
        'rbNormalMsg
        '
        Me.rbNormalMsg.AutoSize = True
        Me.rbNormalMsg.Checked = True
        Me.rbNormalMsg.Location = New System.Drawing.Point(12, 64)
        Me.rbNormalMsg.Name = "rbNormalMsg"
        Me.rbNormalMsg.Size = New System.Drawing.Size(104, 17)
        Me.rbNormalMsg.TabIndex = 3
        Me.rbNormalMsg.TabStop = True
        Me.rbNormalMsg.Text = "Normal Message"
        Me.rbNormalMsg.UseVisualStyleBackColor = True
        '
        'rbWarningMsg
        '
        Me.rbWarningMsg.AutoSize = True
        Me.rbWarningMsg.Location = New System.Drawing.Point(122, 64)
        Me.rbWarningMsg.Name = "rbWarningMsg"
        Me.rbWarningMsg.Size = New System.Drawing.Size(111, 17)
        Me.rbWarningMsg.TabIndex = 4
        Me.rbWarningMsg.Text = "Warning Message"
        Me.rbWarningMsg.UseVisualStyleBackColor = True
        '
        'rbErrorMsg
        '
        Me.rbErrorMsg.AutoSize = True
        Me.rbErrorMsg.Location = New System.Drawing.Point(239, 64)
        Me.rbErrorMsg.Name = "rbErrorMsg"
        Me.rbErrorMsg.Size = New System.Drawing.Size(93, 17)
        Me.rbErrorMsg.TabIndex = 5
        Me.rbErrorMsg.Text = "Error Message"
        Me.rbErrorMsg.UseVisualStyleBackColor = True
        '
        'txtMsg
        '
        Me.txtMsg.Location = New System.Drawing.Point(12, 87)
        Me.txtMsg.Name = "txtMsg"
        Me.txtMsg.Size = New System.Drawing.Size(244, 20)
        Me.txtMsg.TabIndex = 6
        '
        'cmdSend2Log
        '
        Me.cmdSend2Log.Location = New System.Drawing.Point(262, 84)
        Me.cmdSend2Log.Name = "cmdSend2Log"
        Me.cmdSend2Log.Size = New System.Drawing.Size(75, 23)
        Me.cmdSend2Log.TabIndex = 7
        Me.cmdSend2Log.Text = "Send to Log"
        Me.cmdSend2Log.UseVisualStyleBackColor = True
        '
        'cmdSetupMailer
        '
        Me.cmdSetupMailer.Location = New System.Drawing.Point(12, 113)
        Me.cmdSetupMailer.Name = "cmdSetupMailer"
        Me.cmdSetupMailer.Size = New System.Drawing.Size(104, 23)
        Me.cmdSetupMailer.TabIndex = 8
        Me.cmdSetupMailer.Text = "Setup Error Mailer"
        Me.cmdSetupMailer.UseVisualStyleBackColor = True
        '
        'cmdStopMailer
        '
        Me.cmdStopMailer.Location = New System.Drawing.Point(122, 113)
        Me.cmdStopMailer.Name = "cmdStopMailer"
        Me.cmdStopMailer.Size = New System.Drawing.Size(99, 23)
        Me.cmdStopMailer.TabIndex = 9
        Me.cmdStopMailer.Text = "Stop Error Mailer"
        Me.cmdStopMailer.UseVisualStyleBackColor = True
        '
        'lblLogSent
        '
        Me.lblLogSent.AutoSize = True
        Me.lblLogSent.Location = New System.Drawing.Point(192, 40)
        Me.lblLogSent.Name = "lblLogSent"
        Me.lblLogSent.Size = New System.Drawing.Size(65, 13)
        Me.lblLogSent.TabIndex = 10
        Me.lblLogSent.Tag = "Sent at:"
        Me.lblLogSent.Text = "Sent - never"
        '
        'ofdErrMailerConfig
        '
        Me.ofdErrMailerConfig.FileName = "error mailer config"
        Me.ofdErrMailerConfig.Filter = "INI-Files|*.ini"
        Me.ofdErrMailerConfig.Title = "Select INI file to use for mailer config"
        '
        'SimpleLogTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(348, 148)
        Me.Controls.Add(Me.lblLogSent)
        Me.Controls.Add(Me.cmdStopMailer)
        Me.Controls.Add(Me.cmdSetupMailer)
        Me.Controls.Add(Me.cmdSend2Log)
        Me.Controls.Add(Me.txtMsg)
        Me.Controls.Add(Me.rbErrorMsg)
        Me.Controls.Add(Me.rbWarningMsg)
        Me.Controls.Add(Me.rbNormalMsg)
        Me.Controls.Add(Me.cmdRemObject)
        Me.Controls.Add(Me.cmdLoadObject)
        Me.Controls.Add(Me.lblObjectState)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SimpleLogTest"
        Me.Text = "Simple Logfile Test"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblObjectState As Label
    Friend WithEvents cmdLoadObject As Button
    Friend WithEvents cmdRemObject As Button
    Friend WithEvents rbNormalMsg As RadioButton
    Friend WithEvents rbWarningMsg As RadioButton
    Friend WithEvents rbErrorMsg As RadioButton
    Friend WithEvents txtMsg As TextBox
    Friend WithEvents cmdSend2Log As Button
    Friend WithEvents cmdSetupMailer As Button
    Friend WithEvents cmdStopMailer As Button
    Friend WithEvents lblLogSent As Label
    Friend WithEvents ofdErrMailerConfig As OpenFileDialog
End Class
