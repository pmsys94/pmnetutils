﻿Imports pmNETutils.Logging
Public Class DateLogTest
    Inherits SimpleLogTest

    Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub

    Friend Overrides Sub LoadObject()
        LogObj = New DateLogfile
    End Sub

    Private Sub cmdClean_Click(sender As Object, e As EventArgs) Handles cmdClean.Click
        If LogObj Is Nothing Then
            MsgBox("Logfile object is Nothing!", MsgBoxStyle.Critical, "Datelog is nothing")
        Else
            Dim DateLog As DateLogfile = CType(LogObj, DateLogfile)
            DateLog.Cleanup(nudCleanDays.Value)
            lblCleanMsg.Text = "Done cleanup at " & Now.ToLongTimeString()
        End If
    End Sub

    Private Sub cmdPrune_Click(sender As Object, e As EventArgs) Handles cmdPrune.Click
        If LogObj Is Nothing Then
            MsgBox("Logfile object is Nothing!", MsgBoxStyle.Critical, "Datelog is nothing")
        Else
            Dim DateLog As DateLogfile = CType(LogObj, DateLogfile)
            Dim pruned As Integer = DateLog.Prune(nudKeep.Value, chbKeepAbs.Checked)
            lblCleanMsg.Text = "Pruned " & pruned & " logfiles at " & Now.ToLongTimeString()
        End If
    End Sub

    Private Sub StopAutoRotate_Click(sender As Object, e As EventArgs) Handles StopAutoRotate.Click
        If LogObj Is Nothing Then
            MsgBox("Logfile object is Nothing!", MsgBoxStyle.Critical, "Datelog is nothing")
        Else
            Dim DateLog As DateLogfile = CType(LogObj, DateLogfile)
            DateLog.StopAutoCleanup()
        End If
    End Sub

    Private Sub StartAutoRot_At_Click(sender As Object, e As EventArgs) Handles StartAutoRot_At.Click
        If LogObj Is Nothing Then
            MsgBox("Logfile object is Nothing!", MsgBoxStyle.Critical, "Datelog is nothing")
        Else
            Dim DateLog As DateLogfile = CType(LogObj, DateLogfile)
            DateLog.StartCleanupAt(nudCleanDays.Value, AutoRotationAt.BeginOfDay)
        End If
    End Sub

    Private Sub StartAutoRot_Time_Click(sender As Object, e As EventArgs) Handles StartAutoRot_Time.Click
        If LogObj Is Nothing Then
            MsgBox("Logfile object is Nothing!", MsgBoxStyle.Critical, "Datelog is nothing")
        Else
            Dim DateLog As DateLogfile = CType(LogObj, DateLogfile)
            DateLog.StartAutoCleanup(nudCleanDays.Value, 5)
        End If
    End Sub

    Friend WithEvents cmdPrune As Button
    Friend WithEvents lblCleanMsg As Label
    Friend WithEvents nudCleanDays As NumericUpDown
    Friend WithEvents lblCleanDays As Label
    Friend WithEvents lblPruneKeep As Label
    Friend WithEvents nudKeep As NumericUpDown
    Friend WithEvents chbKeepAbs As CheckBox
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents dbtAutoRotate As ToolStripDropDownButton
    Friend WithEvents StopAutoRotate As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents StartAutoRot_At As ToolStripMenuItem
    Friend WithEvents StartAutoRot_Time As ToolStripMenuItem
    Friend WithEvents cmdClean As Button

    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DateLogTest))
        Me.cmdClean = New System.Windows.Forms.Button()
        Me.cmdPrune = New System.Windows.Forms.Button()
        Me.lblCleanMsg = New System.Windows.Forms.Label()
        Me.nudCleanDays = New System.Windows.Forms.NumericUpDown()
        Me.lblCleanDays = New System.Windows.Forms.Label()
        Me.lblPruneKeep = New System.Windows.Forms.Label()
        Me.nudKeep = New System.Windows.Forms.NumericUpDown()
        Me.chbKeepAbs = New System.Windows.Forms.CheckBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.dbtAutoRotate = New System.Windows.Forms.ToolStripDropDownButton()
        Me.StopAutoRotate = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.StartAutoRot_At = New System.Windows.Forms.ToolStripMenuItem()
        Me.StartAutoRot_Time = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.nudCleanDays, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudKeep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdClean
        '
        Me.cmdClean.Location = New System.Drawing.Point(12, 142)
        Me.cmdClean.Name = "cmdClean"
        Me.cmdClean.Size = New System.Drawing.Size(75, 23)
        Me.cmdClean.TabIndex = 12
        Me.cmdClean.Text = "Clean"
        Me.cmdClean.UseVisualStyleBackColor = True
        '
        'cmdPrune
        '
        Me.cmdPrune.Location = New System.Drawing.Point(188, 142)
        Me.cmdPrune.Name = "cmdPrune"
        Me.cmdPrune.Size = New System.Drawing.Size(45, 23)
        Me.cmdPrune.TabIndex = 13
        Me.cmdPrune.Text = "Prune"
        Me.cmdPrune.UseVisualStyleBackColor = True
        '
        'lblCleanMsg
        '
        Me.lblCleanMsg.AutoSize = True
        Me.lblCleanMsg.Location = New System.Drawing.Point(12, 166)
        Me.lblCleanMsg.Name = "lblCleanMsg"
        Me.lblCleanMsg.Size = New System.Drawing.Size(115, 13)
        Me.lblCleanMsg.TabIndex = 14
        Me.lblCleanMsg.Text = "No clean / prune done"
        '
        'nudCleanDays
        '
        Me.nudCleanDays.Location = New System.Drawing.Point(93, 142)
        Me.nudCleanDays.Maximum = New Decimal(New Integer() {366, 0, 0, 0})
        Me.nudCleanDays.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudCleanDays.Name = "nudCleanDays"
        Me.nudCleanDays.Size = New System.Drawing.Size(52, 20)
        Me.nudCleanDays.TabIndex = 15
        Me.nudCleanDays.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'lblCleanDays
        '
        Me.lblCleanDays.AutoSize = True
        Me.lblCleanDays.Location = New System.Drawing.Point(151, 147)
        Me.lblCleanDays.Name = "lblCleanDays"
        Me.lblCleanDays.Size = New System.Drawing.Size(31, 13)
        Me.lblCleanDays.TabIndex = 16
        Me.lblCleanDays.Text = "Days"
        '
        'lblPruneKeep
        '
        Me.lblPruneKeep.AutoSize = True
        Me.lblPruneKeep.Location = New System.Drawing.Point(239, 147)
        Me.lblPruneKeep.Name = "lblPruneKeep"
        Me.lblPruneKeep.Size = New System.Drawing.Size(35, 13)
        Me.lblPruneKeep.TabIndex = 17
        Me.lblPruneKeep.Text = "Keep:"
        '
        'nudKeep
        '
        Me.nudKeep.Location = New System.Drawing.Point(280, 131)
        Me.nudKeep.Maximum = New Decimal(New Integer() {366, 0, 0, 0})
        Me.nudKeep.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudKeep.Name = "nudKeep"
        Me.nudKeep.Size = New System.Drawing.Size(52, 20)
        Me.nudKeep.TabIndex = 15
        Me.nudKeep.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'chbKeepAbs
        '
        Me.chbKeepAbs.AutoSize = True
        Me.chbKeepAbs.Location = New System.Drawing.Point(280, 157)
        Me.chbKeepAbs.Name = "chbKeepAbs"
        Me.chbKeepAbs.Size = New System.Drawing.Size(67, 17)
        Me.chbKeepAbs.TabIndex = 18
        Me.chbKeepAbs.Text = "Absolute"
        Me.chbKeepAbs.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.dbtAutoRotate})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 188)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(348, 22)
        Me.StatusStrip1.TabIndex = 19
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'dbtAutoRotate
        '
        Me.dbtAutoRotate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.dbtAutoRotate.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StopAutoRotate, Me.ToolStripSeparator1, Me.StartAutoRot_At, Me.StartAutoRot_Time})
        Me.dbtAutoRotate.Image = CType(resources.GetObject("dbtAutoRotate.Image"), System.Drawing.Image)
        Me.dbtAutoRotate.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.dbtAutoRotate.Name = "dbtAutoRotate"
        Me.dbtAutoRotate.Size = New System.Drawing.Size(83, 20)
        Me.dbtAutoRotate.Text = "Auto Rotate"
        '
        'StopAutoRotate
        '
        Me.StopAutoRotate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.StopAutoRotate.Name = "StopAutoRotate"
        Me.StopAutoRotate.Size = New System.Drawing.Size(180, 22)
        Me.StopAutoRotate.Text = "Stop"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(177, 6)
        '
        'StartAutoRot_At
        '
        Me.StartAutoRot_At.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.StartAutoRot_At.Name = "StartAutoRot_At"
        Me.StartAutoRot_At.Size = New System.Drawing.Size(180, 22)
        Me.StartAutoRot_At.Text = "Start At"
        '
        'StartAutoRot_Time
        '
        Me.StartAutoRot_Time.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.StartAutoRot_Time.Name = "StartAutoRot_Time"
        Me.StartAutoRot_Time.Size = New System.Drawing.Size(180, 22)
        Me.StartAutoRot_Time.Text = "Start Interval"
        '
        'DateLogTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(348, 210)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.chbKeepAbs)
        Me.Controls.Add(Me.lblPruneKeep)
        Me.Controls.Add(Me.lblCleanDays)
        Me.Controls.Add(Me.nudKeep)
        Me.Controls.Add(Me.nudCleanDays)
        Me.Controls.Add(Me.lblCleanMsg)
        Me.Controls.Add(Me.cmdPrune)
        Me.Controls.Add(Me.cmdClean)
        Me.Name = "DateLogTest"
        Me.Text = "Date Logfile Test"
        Me.Controls.SetChildIndex(Me.cmdClean, 0)
        Me.Controls.SetChildIndex(Me.cmdPrune, 0)
        Me.Controls.SetChildIndex(Me.lblCleanMsg, 0)
        Me.Controls.SetChildIndex(Me.nudCleanDays, 0)
        Me.Controls.SetChildIndex(Me.nudKeep, 0)
        Me.Controls.SetChildIndex(Me.lblCleanDays, 0)
        Me.Controls.SetChildIndex(Me.lblPruneKeep, 0)
        Me.Controls.SetChildIndex(Me.chbKeepAbs, 0)
        Me.Controls.SetChildIndex(Me.StatusStrip1, 0)
        CType(Me.nudCleanDays, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudKeep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
End Class