﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class INITest
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdLoadFile = New System.Windows.Forms.Button()
        Me.cmdSelPutLoc = New System.Windows.Forms.Button()
        Me.lblFilePath = New System.Windows.Forms.Label()
        Me.grpSections = New System.Windows.Forms.GroupBox()
        Me.cmdTriggerHasSections = New System.Windows.Forms.Button()
        Me.lstSections = New System.Windows.Forms.ListBox()
        Me.KVGrid = New System.Windows.Forms.DataGridView()
        Me.grpSectionData = New System.Windows.Forms.GroupBox()
        Me.cmdAddValue = New System.Windows.Forms.Button()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.sfd = New System.Windows.Forms.SaveFileDialog()
        Me.lblDefaultGet = New System.Windows.Forms.Label()
        Me.txtDefault = New System.Windows.Forms.TextBox()
        Me.cmdCheckExists = New System.Windows.Forms.Button()
        Me.colKeyName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colValue = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grpSections.SuspendLayout()
        CType(Me.KVGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpSectionData.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdLoadFile
        '
        Me.cmdLoadFile.Location = New System.Drawing.Point(12, 12)
        Me.cmdLoadFile.Name = "cmdLoadFile"
        Me.cmdLoadFile.Size = New System.Drawing.Size(75, 23)
        Me.cmdLoadFile.TabIndex = 0
        Me.cmdLoadFile.Text = "Load"
        Me.cmdLoadFile.UseVisualStyleBackColor = True
        '
        'cmdSelPutLoc
        '
        Me.cmdSelPutLoc.Location = New System.Drawing.Point(12, 41)
        Me.cmdSelPutLoc.Name = "cmdSelPutLoc"
        Me.cmdSelPutLoc.Size = New System.Drawing.Size(109, 23)
        Me.cmdSelPutLoc.TabIndex = 1
        Me.cmdSelPutLoc.Text = "Select Path to put"
        Me.cmdSelPutLoc.UseVisualStyleBackColor = True
        '
        'lblFilePath
        '
        Me.lblFilePath.AutoSize = True
        Me.lblFilePath.Location = New System.Drawing.Point(93, 17)
        Me.lblFilePath.Name = "lblFilePath"
        Me.lblFilePath.Size = New System.Drawing.Size(104, 13)
        Me.lblFilePath.TabIndex = 2
        Me.lblFilePath.Text = "No file path selected"
        '
        'grpSections
        '
        Me.grpSections.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpSections.Controls.Add(Me.cmdTriggerHasSections)
        Me.grpSections.Controls.Add(Me.lstSections)
        Me.grpSections.Location = New System.Drawing.Point(3, 70)
        Me.grpSections.Name = "grpSections"
        Me.grpSections.Size = New System.Drawing.Size(194, 379)
        Me.grpSections.TabIndex = 3
        Me.grpSections.TabStop = False
        Me.grpSections.Text = "Sections"
        '
        'cmdTriggerHasSections
        '
        Me.cmdTriggerHasSections.Location = New System.Drawing.Point(8, 17)
        Me.cmdTriggerHasSections.Name = "cmdTriggerHasSections"
        Me.cmdTriggerHasSections.Size = New System.Drawing.Size(130, 23)
        Me.cmdTriggerHasSections.TabIndex = 1
        Me.cmdTriggerHasSections.Text = "Trigger HasSections"
        Me.cmdTriggerHasSections.UseVisualStyleBackColor = True
        '
        'lstSections
        '
        Me.lstSections.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstSections.FormattingEnabled = True
        Me.lstSections.Location = New System.Drawing.Point(6, 41)
        Me.lstSections.Name = "lstSections"
        Me.lstSections.Size = New System.Drawing.Size(182, 329)
        Me.lstSections.TabIndex = 0
        '
        'KVGrid
        '
        Me.KVGrid.AllowUserToAddRows = False
        Me.KVGrid.AllowUserToDeleteRows = False
        Me.KVGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.KVGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.KVGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colKeyName, Me.colValue})
        Me.KVGrid.Location = New System.Drawing.Point(3, 41)
        Me.KVGrid.Name = "KVGrid"
        Me.KVGrid.ShowEditingIcon = False
        Me.KVGrid.Size = New System.Drawing.Size(597, 335)
        Me.KVGrid.TabIndex = 4
        '
        'grpSectionData
        '
        Me.grpSectionData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpSectionData.Controls.Add(Me.cmdAddValue)
        Me.grpSectionData.Controls.Add(Me.KVGrid)
        Me.grpSectionData.Location = New System.Drawing.Point(200, 70)
        Me.grpSectionData.Name = "grpSectionData"
        Me.grpSectionData.Size = New System.Drawing.Size(600, 376)
        Me.grpSectionData.TabIndex = 5
        Me.grpSectionData.TabStop = False
        Me.grpSectionData.Text = "No selected Section"
        '
        'cmdAddValue
        '
        Me.cmdAddValue.Enabled = False
        Me.cmdAddValue.Location = New System.Drawing.Point(6, 16)
        Me.cmdAddValue.Name = "cmdAddValue"
        Me.cmdAddValue.Size = New System.Drawing.Size(75, 23)
        Me.cmdAddValue.TabIndex = 5
        Me.cmdAddValue.Text = "Add Value"
        Me.cmdAddValue.UseVisualStyleBackColor = True
        '
        'ofd
        '
        Me.ofd.Filter = "INI Files|*.ini;*.inf"
        Me.ofd.Title = "Select File to load into test"
        '
        'sfd
        '
        Me.sfd.Filter = "INI-File|*.ini|Setup-Config|*.inf"
        '
        'lblDefaultGet
        '
        Me.lblDefaultGet.AutoSize = True
        Me.lblDefaultGet.Location = New System.Drawing.Point(224, 46)
        Me.lblDefaultGet.Name = "lblDefaultGet"
        Me.lblDefaultGet.Size = New System.Drawing.Size(118, 13)
        Me.lblDefaultGet.TabIndex = 6
        Me.lblDefaultGet.Text = "Default for getting data:"
        '
        'txtDefault
        '
        Me.txtDefault.Location = New System.Drawing.Point(348, 43)
        Me.txtDefault.Name = "txtDefault"
        Me.txtDefault.Size = New System.Drawing.Size(232, 20)
        Me.txtDefault.TabIndex = 7
        '
        'cmdCheckExists
        '
        Me.cmdCheckExists.Enabled = False
        Me.cmdCheckExists.Location = New System.Drawing.Point(586, 41)
        Me.cmdCheckExists.Name = "cmdCheckExists"
        Me.cmdCheckExists.Size = New System.Drawing.Size(96, 23)
        Me.cmdCheckExists.TabIndex = 8
        Me.cmdCheckExists.Text = "Check Exists"
        Me.cmdCheckExists.UseVisualStyleBackColor = True
        '
        'colKeyName
        '
        Me.colKeyName.Frozen = True
        Me.colKeyName.HeaderText = "Key"
        Me.colKeyName.Name = "colKeyName"
        Me.colKeyName.ReadOnly = True
        Me.colKeyName.Width = 200
        '
        'colValue
        '
        Me.colValue.Frozen = True
        Me.colValue.HeaderText = "Value"
        Me.colValue.Name = "colValue"
        Me.colValue.Width = 350
        '
        'INITest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.cmdCheckExists)
        Me.Controls.Add(Me.txtDefault)
        Me.Controls.Add(Me.lblDefaultGet)
        Me.Controls.Add(Me.grpSectionData)
        Me.Controls.Add(Me.grpSections)
        Me.Controls.Add(Me.lblFilePath)
        Me.Controls.Add(Me.cmdSelPutLoc)
        Me.Controls.Add(Me.cmdLoadFile)
        Me.Name = "INITest"
        Me.Text = "INI File Test"
        Me.grpSections.ResumeLayout(False)
        CType(Me.KVGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpSectionData.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cmdLoadFile As Button
    Friend WithEvents cmdSelPutLoc As Button
    Friend WithEvents lblFilePath As Label
    Friend WithEvents grpSections As GroupBox
    Friend WithEvents lstSections As ListBox
    Friend WithEvents KVGrid As DataGridView
    Friend WithEvents grpSectionData As GroupBox
    Friend WithEvents cmdAddValue As Button
    Friend WithEvents ofd As OpenFileDialog
    Friend WithEvents sfd As SaveFileDialog
    Friend WithEvents lblDefaultGet As Label
    Friend WithEvents txtDefault As TextBox
    Friend WithEvents cmdCheckExists As Button
    Friend WithEvents cmdTriggerHasSections As Button
    Friend WithEvents colKeyName As DataGridViewTextBoxColumn
    Friend WithEvents colValue As DataGridViewTextBoxColumn
End Class
