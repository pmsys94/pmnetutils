﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TestSelectorForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdLogTest = New System.Windows.Forms.Button()
        Me.cmdINI = New System.Windows.Forms.Button()
        Me.cmdMailer = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdLogTest
        '
        Me.cmdLogTest.Location = New System.Drawing.Point(3, 3)
        Me.cmdLogTest.Name = "cmdLogTest"
        Me.cmdLogTest.Size = New System.Drawing.Size(75, 23)
        Me.cmdLogTest.TabIndex = 0
        Me.cmdLogTest.Text = "Logfiles"
        Me.cmdLogTest.UseVisualStyleBackColor = True
        '
        'cmdINI
        '
        Me.cmdINI.Location = New System.Drawing.Point(84, 3)
        Me.cmdINI.Name = "cmdINI"
        Me.cmdINI.Size = New System.Drawing.Size(75, 23)
        Me.cmdINI.TabIndex = 1
        Me.cmdINI.Text = "INI Files"
        Me.cmdINI.UseVisualStyleBackColor = True
        '
        'cmdMailer
        '
        Me.cmdMailer.Location = New System.Drawing.Point(165, 3)
        Me.cmdMailer.Name = "cmdMailer"
        Me.cmdMailer.Size = New System.Drawing.Size(75, 23)
        Me.cmdMailer.TabIndex = 2
        Me.cmdMailer.Text = "Mailer"
        Me.cmdMailer.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdLogTest)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdINI)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdMailer)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(249, 51)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'TestSelectorForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(249, 51)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "TestSelectorForm"
        Me.Text = "pmNETutils Test App"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents cmdLogTest As Button
    Friend WithEvents cmdINI As Button
    Friend WithEvents cmdMailer As Button
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
End Class
