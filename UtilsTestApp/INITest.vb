﻿Imports pmNETutils.Configurations.INI
Public Class INITest
    Private file As INI_File
    Private section As INI_Section

    Private Sub INITest_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        TestSelectorForm.Show()
    End Sub

    Private Sub cmdLoadFile_Click(sender As Object, e As EventArgs) Handles cmdLoadFile.Click
        If ofd.ShowDialog(Me) = DialogResult.OK Then
            file = INI_File.OpenINIfile(ofd.FileName, txtDefault.Text)
            lblFilePath.Text = file.FilePath
            FileActions = True
            For Each sec As String In file.Sections
                lstSections.Items.Add(sec)
            Next
        End If
    End Sub

    Private Property FileActions As Boolean
        Get
            Return cmdAddValue.Enabled
        End Get
        Set(value As Boolean)
            ' disable buttons
            For Each cmd As Button In {cmdLoadFile, cmdSelPutLoc}
                cmd.Enabled = Not value
            Next
            ' enable buttons
            For Each cmd As Button In {cmdCheckExists, cmdAddValue, cmdTriggerHasSections}
                cmd.Enabled = value
            Next
        End Set
    End Property

    Private Sub cmdTriggerHasSections_Click(sender As Object, e As EventArgs) Handles cmdTriggerHasSections.Click
        Const boxtitle As String = "Check if the HasSection function works"
        Dim secname As String = InputBox("Enter the section name to lookup.", boxtitle)
        If secname.Length > 0 Then
            If file.HasSection(secname) Then
                MsgBox("Section '" & secname & "' was found!", MsgBoxStyle.Information, boxtitle)
            Else
                MsgBox("Section '" & secname & "' was not found!", MsgBoxStyle.Information, boxtitle)
            End If
        End If
    End Sub

    Private Sub cmdSelPutLoc_Click(sender As Object, e As EventArgs) Handles cmdSelPutLoc.Click
        If sfd.ShowDialog() = DialogResult.OK Then
            file = New INI_File(sfd.FileName, txtDefault.Text)
            section = Nothing
            lblFilePath.Text = file.FilePath
            FileActions = True
        End If
    End Sub

    Private Sub lstSections_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstSections.SelectedIndexChanged
        KVGrid.Rows.Clear()
        Dim row_idx As Integer
        Dim row As DataGridViewRow
        Dim val As String
        section = file.GetSection(lstSections.SelectedItem)
        grpSectionData.Text = "Section: " & section.Name
        For Each keyname As String In section.Keys
            row_idx = KVGrid.Rows.Add()
            row = KVGrid.Rows.Item(row_idx)
            val = section.Value(keyname)
            row.Cells(0).Value = keyname
            row.Cells(1).Value = val
        Next
    End Sub

    Private Sub cmdCheckExists_Click(sender As Object, e As EventArgs) Handles cmdCheckExists.Click
        If file.Exists Then
            MsgBox("File exists", MsgBoxStyle.Information, "Check INI file exits")
        Else
            MsgBox("File does not exist", MsgBoxStyle.Information, "Check INI file exists")
        End If
    End Sub

    Private Sub cmdAddValue_Click(sender As Object, e As EventArgs) Handles cmdAddValue.Click
        Dim secname, keyname, val As String
        Dim row_idx As Integer
        Dim row As DataGridViewRow
        If section Is Nothing Then
            secname = InputBox("No Section. Please enter a name for section to create.", "Add Value - No Section")
            If secname.Length = 0 Then
                MsgBox("No Section name given. Needed to add a value to file.", MsgBoxStyle.Exclamation, "Add Value - No Sectionname")
                Exit Sub
            ElseIf file.HasSection(secname) Then
                MsgBox("Entered Section Name '" & secname & "' is already in file. Need a diffrent Name.", MsgBoxStyle.Exclamation, "Add Value - Existing sectionname")
                Exit Sub
            End If
        Else
            If MsgBox("Add value to section which currently not exists?", MsgBoxStyle.YesNo, "Add Value - Existing selected section") = MsgBoxResult.Yes Then
                secname = InputBox("Tell the name of section to add.", "Add Value - add with new section")
                If file.HasSection(secname) Then
                    MsgBox("Sorry, the section name '" & secname & "' is already in file.", MsgBoxStyle.Exclamation, "Add value - add with new section")
                    Exit Sub
                End If
            Else
                secname = ""
            End If
        End If
        keyname = InputBox("Please enter a keyname for the value.", "Add Value - keyname")
        If keyname.Length > 0 Then
            If secname.Length = 0 AndAlso section.HasKey(keyname) Then
                MsgBox("The keyname you entered ('" & keyname & "') is already in the selected section.", MsgBoxStyle.Exclamation, "New Value - Keyname")
                Exit Sub
            End If
        Else
            MsgBox("Keyname is required to add a vulue.", MsgBoxStyle.Exclamation, "Add Value - keyname")
        End If
        val = InputBox("Please enter the value of " & keyname, "New value")
        If secname.Length > 0 Then
            file.Item(secname, keyname) = val
            lstSections.Items.Clear()
            For Each sec As String In file.Sections
                lstSections.Items.Add(sec)
            Next
            lstSections.SelectedItem = secname
        Else
            section.Value(keyname) = val
            row_idx = KVGrid.Rows.Add()
            row = KVGrid.Rows(row_idx)
            row.Cells(0).Value = keyname
            row.Cells(1).Value = val
        End If
    End Sub

    Private Sub KVGrid_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles KVGrid.CellEndEdit
        Dim row As DataGridViewRow = KVGrid.Rows(e.RowIndex)
        Dim keyname As String = CStr(row.Cells(0).Value)
        section.Value(keyname) = CStr(row.Cells(1).Value)
    End Sub
End Class