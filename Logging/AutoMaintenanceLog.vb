﻿Imports System.Timers
Namespace Logging
    Public MustInherit Class AutoMaintenanceLog
        Inherits Logfile
        Friend tim As Timer
        Friend hours, cnt As Integer
        Friend WrLock As Boolean = False
        Private SyncronizeTo As Boolean = False
        Friend SyncronizeMode As AutoRotationAt

        Friend Sub New(Optional ld As String = "", Optional fext As String = "", Optional create As Boolean = True)
            MyBase.New(ld, fext, create)
        End Sub

        Protected Overrides Sub Finalize()
            If tim IsNot Nothing Then
                tim.Stop()
                tim.Dispose()
            End If
            MyBase.Finalize()
        End Sub

        Friend Sub StartTimer(hours As Integer, Optional sync As AutoRotationAt = AutoRotationAt.Timeout)
            Dim syncDiff As Integer ' sync diffrence to the next tick at full hour
            If Not sync = AutoRotationAt.Timeout Then
                syncDiff = (((59 - Now.Minute) * 60) + (60 - Now.Second)) * 1000 ' (( 59sec á 1min - Min) * 60) á sec + (60 - sec) -> *1000 á msec
                tim = New Timer(syncDiff)
                SyncronizeTo = True
                ' diffrence to know how many hours to count when set by AutoRotationAt != Timeout
                hours = 24 - Now.AddMilliseconds(syncDiff).Hour ' hours to next begin of day
                If hours = 24 Then hours = 0
                Me.hours = hours
            Else
                Me.hours = hours
                tim = New Timer(3600000) ' set to get each relative hour
            End If
            SyncronizeMode = sync
            AddHandler tim.Elapsed, AddressOf tim_Elapsed
            tim.AutoReset = True
            cnt = 0
            tim.Start()
        End Sub

        Private Function Diff2NextSunday(dow As DayOfWeek) As Integer
            Return 7 - dow
        End Function

        Private Function Diff2NextMonday(dow As DayOfWeek) As Integer
            Dim diff As Integer
            If dow = DayOfWeek.Sunday Then
                diff = 0
            Else
                diff = Diff2NextSunday(dow)
            End If
            Return diff + 1
        End Function

        Friend Sub StopTimer()
            tim.Stop()
            tim.Dispose()
            tim = Nothing
        End Sub

        Friend MustOverride Sub TimerEvent()

        Private Sub tim_Elapsed(sender As Object, e As ElapsedEventArgs)
            cnt += 1
            If cnt >= hours Then
                cnt = 0
                If Not SyncronizeMode = AutoRotationAt.Timeout And SyncronizeTo Then
                    SyncronizeTo = False
                    ' assume in this case that were at 00:00 (AM) because start routine has setup hours relative to begin of day (of a week | month | year)
                    Select Case SyncronizeMode
                        Case AutoRotationAt.BeginOfDay
                            hours = 24 ' hours -> count to begin of day
                        Case AutoRotationAt.BeginOfWeek
                            If Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek = DayOfWeek.Monday Then
                                hours = Diff2NextMonday(Today.DayOfWeek) * 24
                                If Diff2NextMonday(Today.DayOfWeek) < 7 Then SyncronizeTo = True
                            Else ' sunday
                                hours = Diff2NextSunday(Today.DayOfWeek) * 24
                                If Diff2NextSunday(Today.DayOfWeek) < 7 Then SyncronizeTo = True
                            End If
                        Case AutoRotationAt.BeginOfMonth
                            hours = CInt((New Date(Today.Year, Today.Month + 1, 1, 0, 0, 0) - Now).TotalHours)
                            If Not Today.Day = 1 Then SyncronizeTo = True
                        Case AutoRotationAt.BeginOfYear
                            hours = CInt((New Date(Today.Year + 1, 1, 1, 0, 0, 0) - Now).TotalHours)
                            If Not (Today.Day = 1 And Today.Month = 1) Then SyncronizeTo = True
                    End Select
                    If tim.Interval < 3600000 Then tim.Interval = 3600000 ' Update interval msec to tick now every full hour ! we are on this tick at a full hour
                End If
                If Not SyncronizeTo Then
                    WrLock = True
                    TimerEvent()
                    WrLock = False
                End If
            Else
                If SyncronizeTo Then
                    If tim.Interval < 3600000 Then tim.Interval = 3600000 ' 3 Mil. 600Th. MSec. -> 1hour
                End If
            End If
        End Sub

        Friend ReadOnly Property Interval As Integer
            Get
                Return hours
            End Get
        End Property

        Public Overrides Sub NormalLine(ln As String)
            If Not WrLock Then MyBase.NormalLine(ln)
        End Sub

        Public Overrides Sub WarningLine(ln As String)
            If Not WrLock Then
                MyBase.WarningLine(ln)
            Else
                Throw New Exception("Warning write while Logfile is locked for rotation")
            End If
        End Sub

        Public Overrides Sub ErrorLine(ln As String)
            If Not WrLock Then
                MyBase.ErrorLine(ln)
            Else
                Throw New Exception("Error write while Logfile is locked for rotation")
            End If
        End Sub

    End Class

    Public Enum AutoRotationAt
        Timeout
        BeginOfDay
        BeginOfWeek
        BeginOfMonth
        BeginOfYear
    End Enum

End Namespace