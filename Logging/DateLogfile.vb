﻿Imports System.IO
Namespace Logging
    Public Class DateLogfile
        Inherits AutoMaintenanceLog

        Private xd As Integer = 0

        ''' <summary>
        ''' Creates a Logfile with a Date as name extension. It can be cleaned by date. 
        ''' </summary>
        ''' <param name="ld">Path to store Logfile. The Path must not end with a backslash.</param>
        Sub New(Optional ld As String = "")
            MyBase.New(ld, "_" & Now.ToString("yyyy-MM-dd") & ".log")
        End Sub

        Protected Overrides Sub finalize()
            MyBase.Finalize()
        End Sub

        ''' <summary>
        ''' Compares the file dates and removes them if they are older X days.
        ''' </summary>
        ''' <param name="Older_X_Days">Number of days a file must be older. Default is 30.</param>
        ''' <returns>-1 on error, else Number of removed files.</returns>
        Public Function Cleanup(Optional Older_X_Days As Integer = 30) As Integer
            Dim cnt As Integer = 0
            Dim di As New DirectoryInfo(logdir)
            Dim allLogs As List(Of FileInfo) = di.GetFiles("*.log").ToList
            Try
                For Each file As FileInfo In allLogs
                    If (Today - file.CreationTime).Days >= Older_X_Days Then
                        file.Delete()
                        cnt += 1
                    End If
                Next
            Catch ex As Exception
                WrLine("@logger (Cleanup) ***ERROR: " & ex.Message)
                cnt = -1
            End Try
            Return cnt
        End Function
        ''' <summary>
        ''' Removes last N(+1) logfiles. 
        ''' Use <para>keep</para> to altenate the number of keept files.
        ''' Use <para>KeepAbsolute</para> to alternate if <para>keep</para> includes this file.
        ''' If <para>keep</para> is 0 and <para>KeepAbsolute</para> is True then nothing will be done and 0 will be returned.
        ''' </summary>
        ''' <param name="keep">How many files should stay on filesystem after prune.
        ''' Use <para>KeepAbsolute</para> to tell if this number excludes the current file.</param>
        ''' <param name="KeepAbsolute">Let <para>keep</para> mean that absolute the number incl. the current file will be keept.</param>
        ''' <returns>-1 on error, else Number of removed files.</returns>
        Public Function Prune(Optional keep As Integer = 30, Optional KeepAbsolute As Boolean = False) As Integer
            If keep < 0 Then keep = 0
            If Not KeepAbsolute Then
                keep += 1
            ElseIf keep = 0 Then ' on this condition KeepAbsolute must be True
                Return keep
            End If
            Dim cnt As Integer = 0
            Dim di As New DirectoryInfo(logdir)
            Dim allLogs As List(Of FileInfo) = di.GetFiles("*.log").ToList
            Try
                If allLogs.Count > keep Then
                    allLogs.Sort(Function(x As FileInfo, y As FileInfo) y.CreationTime.CompareTo(x.CreationTime))
                    For i As Integer = keep To allLogs.Count - 1
                        allLogs.Item(i).Delete()
                        cnt += 1
                    Next
                End If
            Catch ex As Exception
                WrLine("@logger (Prune) ***ERROR: " & ex.Message)
                Return -1
            End Try
            Return cnt
        End Function

        Friend Overrides Sub TimerEvent()
            ' log to last file the rotation
            WrLine("Rotating logfile.")
            ' rewrite file name
            fext = "_" & Now.ToString("yyyy-MM-dd") & ".log"
            fn_short = My.Application.Info.AssemblyName & fext
            ' create the new file
            If Not File.Exists(fn) Then CreateFile(logdir, fn, True)
            ' cleanup after rotation
            Cleanup(xd)
        End Sub

        Public ReadOnly Property AutoCleanupInterval As Integer
            Get
                Return Interval
            End Get
        End Property

        Public ReadOnly Property AutoCleanupDays As Integer
            Get
                Return xd
            End Get
        End Property

        Public ReadOnly Property AutoCleanAt As AutoRotationAt
            Get
                Return SyncronizeMode
            End Get
        End Property

        Public Sub StartAutoCleanup(olderDays As Integer, CleanIntervalHours As Integer)
            xd = olderDays
            StartTimer(CleanIntervalHours)
        End Sub

        Public Sub StartCleanupAt(olderDays As Integer, at As AutoRotationAt)
            xd = olderDays
            StartTimer(0, at)
        End Sub

        Public Sub StopAutoCleanup()
            StopTimer()
        End Sub

    End Class
End Namespace