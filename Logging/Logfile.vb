﻿Imports System.IO
Namespace Logging
    Public Class Logfile
        Friend logdir As String = My.Application.Info.DirectoryPath & "\log"
        Friend ReadOnly Property fn As String
            Get
                Return logdir & "\" & fn_short
            End Get
        End Property
        Friend fn_short As String
        Friend fext As String
        Protected Friend mailer As ErrMailer = Nothing
        Public ReadOnly Property ErrorMailer As ErrMailer
            Get
                Return mailer
            End Get
        End Property
        ''' <summary>
        ''' Creates a new simple Logfile. Use Arguments to change path and name postfix.
        ''' </summary>
        ''' <param name="ld">Path to store Logfile. The Path must not end with a backslash.</param>
        ''' <param name="fext">Postfix to append to Logfile.</param>
        ''' <param name="create">Dont create the file in sub New()</param>
        Sub New(Optional ld As String = "", Optional fext As String = "", Optional create As Boolean = True)
            fn_short = My.Application.Info.AssemblyName
            If ld.Length > 0 Then logdir = ld
            If fext.Length > 0 Then
                Me.fext = fext
                fn_short &= fext
            Else
                Me.fext = ".log"
                fn_short &= ".log"
            End If
            If create Then CreateFile(logdir, fn, False)
        End Sub

        Public Sub SetupMailer(conf As ErrMailerConfig, Optional FromAddr As String = "", Optional ToAddr As String() = Nothing)
            mailer = ErrMailer.Init(conf, Me, FromAddr, ToAddr)
        End Sub

        Public Sub StopMailer(Optional AttachLog As Boolean = False)
            If mailer Is Nothing Then Exit Sub
            If AttachLog Then
                Dim f As New FileInfo(fn)
                mailer.StopMailer(f)
            Else
                mailer.StopMailer()
            End If
            mailer = Nothing
        End Sub

        Protected Shared Sub CreateFile(ld As String, fn As String, rotation As Boolean)
            Dim d As New DirectoryInfo(ld)
            If Not d.Exists Then d.Create()
            Dim f = New FileInfo(fn)
            Dim alreadyThere As Boolean = f.Exists
            Dim fstrm As FileStream
            Dim line As String = Now.ToString("yyyy-MM-dd HH:mm:ss")
            If Not alreadyThere Then
                fstrm = f.Create()
                fstrm.Close()
            End If
            Using wr = f.AppendText()
                If Not alreadyThere Then
                    If rotation Then
                        wr.WriteLine(line & " Logfile begins after rotation.")
                    Else
                        wr.WriteLine(line & " Logfile Created for Application")
                    End If
                End If
                If Not rotation Then wr.WriteLine(line & " Startup " & My.Application.Info.AssemblyName & " Version String: " & My.Application.Info.Version.ToString)
                wr.Close()
            End Using
        End Sub

        Friend Function WrLine(ByRef ln As String, Optional annot As String = "") As String
            Dim f = New FileInfo(fn)
            Dim line As String = FormatLogLine(ln, annot)
            Using wr = f.AppendText()
                wr.WriteLine(line)
                wr.Close()
            End Using
            Return line
        End Function

        Friend Shared Function FormatLogLine(ByRef ln As String, Optional annot As String = "") As String
            Dim line As String = Now.ToString("yyyy-MM-dd HH:mm:ss")
            line &= annot
            line &= " " & ln
            Return line
        End Function

        ''' <summary>
        ''' Writes the given text line to the logfile prefixed with date and time.
        ''' </summary>
        ''' <param name="ln">Text to write into log.</param>
        Public Overridable Sub NormalLine(ln As String)
            WrLine(ln)
        End Sub
        ''' <summary>
        ''' Weites a error text into the logfile prefixed with date and time when the error occured and ***ERROR to mark it in text
        ''' </summary>
        ''' <param name="ln">Error Text to write in logfile</param>
        Public Overridable Sub ErrorLine(ln As String)
            Dim line As String = WrLine(ln, " ***ERROR:")
            If mailer IsNot Nothing Then mailer.Add(line)
        End Sub
        ''' <summary>
        ''' Writes a warning into the logfile prefixed with date and time and the ***WARNING prefix to mark it in text
        ''' </summary>
        ''' <param name="ln">Warning message to write into logfile</param>
        Public Overridable Sub WarningLine(ln As String)
            Dim line As String = WrLine(ln, "***WARNING:")
            If mailer IsNot Nothing Then
                If mailer.CollectWarnings Then mailer.Add(line)
            End If
        End Sub

        Protected Overrides Sub Finalize()
            NormalLine("----Application terminates----")
            MyBase.Finalize()
        End Sub
    End Class
End Namespace