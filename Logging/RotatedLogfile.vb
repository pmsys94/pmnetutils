﻿Imports System.IO
Namespace Logging
    Public Class RotatedLogfile
        Inherits AutoMaintenanceLog
        Private keep As Integer

        Private Sub New(keep As Integer, Optional ld As String = "")
            MyBase.New(ld,, False)
            Me.keep = keep
        End Sub

        Protected Overrides Sub finalize()
            MyBase.Finalize()
        End Sub
        ''' <summary>
        ''' Creates a Logfile with a Numbering as name extension. The files are rotated automaticaly.
        ''' </summary>
        ''' <param name="keep">Number of files to keep on disk before remove.</param>
        ''' <param name="ld">Path to store Logfile. The Path must not end with a backslash.</param>
        Public Shared Function Create(keep As Integer, Optional ld As String = "") As RotatedLogfile
            Dim lf As New RotatedLogfile(keep, ld)
            RotateLogfile(lf)
            CreateFile(lf.logdir, lf.fn, False)
            Return lf
        End Function

        ''' <summary>
        ''' Rotates all logfiles after the zero'd revision of logfile. Will rename all files after MyApp.log and remove older revisions. 
        ''' </summary>
        ''' <param name="lf">The zero'd logfile revision e.g MyApp.log will be MyApp.1.log and so on.</param>
        Public Shared Sub RotateLogfile(ByRef lf As RotatedLogfile)
            Dim di As New DirectoryInfo(lf.logdir)
            Dim renLogs As New SortedDictionary(Of Integer, FileInfo)
            Dim renLog As FileInfo = Nothing
            Dim logrev As Integer
            Dim logrev_str As String
            Dim lastlog As FileInfo = Nothing
            Dim allLogs As List(Of FileInfo) = di.GetFiles("*.log").ToList
            For Each file As FileInfo In allLogs
                If IsNumeric(file.Name.Substring(file.Name.Length - 5, 1)) Then
                    logrev_str = file.Name.Substring(file.Name.IndexOf(".") + 1) ' MyApp.23.log -> 23.log !== MyApp.1.log -> 1.log
                    logrev_str = logrev_str.Remove(logrev_str.IndexOf(".")) ' 23.log -> 23
                    logrev = Integer.Parse(logrev_str)
                    ' assume MyApp.1.log ... MyApp.29.log
                    If logrev < lf.keep - 1 Then ' 1..28
                        renLogs.Add(logrev, file)
                    Else ' 29>
                        file.Delete()
                    End If
                Else
                    lastlog = file
                End If
            Next
            ' reverse rename all keept logs to the next iterration
            ' assume MyApp.28.log is first item to item MyApp.1.log
            For Each rev_f As KeyValuePair(Of Integer, FileInfo) In renLogs.Reverse()
                logrev = rev_f.Key ' 28..1
                renLog = rev_f.Value ' MyApp.28.log ... MyApp.1.log
                logrev_str = renLog.Name.Remove(renLog.Name.IndexOf(".")) ' MyApp.28.log -> MyApp <- remove .28.log
                renLog.MoveTo(renLog.DirectoryName & "\" & logrev_str & "." & logrev + 1 & ".log") ' when file name = MyApp.28.log -> MyApp.29.log
            Next
            If lastlog IsNot Nothing Then
                ' rename the last log to revision .1.log
                logrev_str = lastlog.Name.Remove(lastlog.Name.IndexOf("."))
                lastlog.MoveTo(lastlog.DirectoryName & "\" & logrev_str & ".1" & ".log")
            End If
        End Sub

        ''' <summary>
        ''' Rotate logfile beginning from this logfile as revision zero. 
        ''' A revision MyApp.1.log will be renamed to MyApp.2.log. 
        ''' Nothing will be done on the this instance of logfile.
        ''' </summary>
        Public Sub Rotate()
            RotateLogfile(Me)
        End Sub

        Friend Overrides Sub TimerEvent()
            ' log to last file the rotation
            WrLine("Rotating logfile. All messages will continue going into the new file.")
            ' now rotate all files
            RotateLogfile(Me)
            ' create a new one
            CreateFile(logdir, fn, True)
        End Sub

        Public ReadOnly Property AutoRotationInterval As Integer
            Get
                Return Interval
            End Get
        End Property

        Public ReadOnly Property AutoRotateAt As AutoRotationAt
            Get
                Return SyncronizeMode
            End Get
        End Property

        Public Sub StartAutoRotation(RotationHourInverval As Integer)
            StartTimer(RotationHourInverval)
        End Sub

        Public Sub StartRotationAt(at As AutoRotationAt)
            StartTimer(0, at)
        End Sub

        Public Sub StopAutoRotation()
            StopTimer()
        End Sub

    End Class
End Namespace