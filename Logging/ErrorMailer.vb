﻿Namespace Logging

    Public Class ErrMailerConfig
        Public baseConfig As Mailing.MailerConfig
        Public FromAddr As String = ""
        Public ToAddr As String = ""

        Private Sub New()
            MyBase.New()
        End Sub

        Public Shared Function FormINIFile_wKnownKeys(FullPath As String, section As String, wCred As Boolean) As ErrMailerConfig
            Dim ini As Configurations.INI.INI_File = Configurations.INI.INI_File.OpenINIfile(FullPath)
            If Not ini.Exists Then Throw New Exception("Given INI file full path does not exist. Unable to load mailer config")
            If Not ini.HasSection(section) Then Throw New Exception("Given INI section does not exist. Unable to load mailer config from file.")
            Return FromINISection(ini.GetSection(section), wCred)
        End Function

        Public Shared Function FromINISection(section As Configurations.INI.INI_Section, wCred As Boolean) As ErrMailerConfig
            Dim conf As New ErrMailerConfig
            If wCred Then
                conf.baseConfig = Mailing.MailerConfig_wCred.FromINISection(section)
            Else
                conf.baseConfig = Mailing.MailerConfig.FromINISection(section)
            End If
            If section.HasKey("EFrom") Then conf.FromAddr = section.Value("EFrom")
            If section.HasKey("ETo") Then conf.ToAddr = section.Value("ETo")
            Return conf
        End Function
    End Class

    Public Class ErrMailer
        Inherits Mailing.Mailer
        Public Enum AutoMailMode
            NoAutomaticMails
            ErrorSummary
            OnEachError
        End Enum

        Private Shared instance As ErrMailer = Nothing

        Public ErrorMailMode As AutoMailMode = AutoMailMode.ErrorSummary
        Private errs As New SortedDictionary(Of Integer, String)
        Private mail As Mailing.PreparedMail
        Private ParentLogfile As Logfile
        Public CollectWarnings As Boolean = False
        Private Sub New(conf As Mailing.MailerConfig, Optional lfo As Logfile = Nothing)
            MyBase.New(conf)
            ParentLogfile = lfo
        End Sub

        Protected Overrides Sub Finalize()
            instance = Nothing
            MyBase.Finalize()
        End Sub

        Public ReadOnly Property ErrorLines As String()
            Get
                Return errs.Values.ToArray()
            End Get
        End Property
        Public ReadOnly Property MailTemplate As Mailing.PreparedMail
            Get
                Return mail
            End Get
        End Property

        Public Shared Function Init(cnf As ErrMailerConfig, Optional FromAddr As String = "", Optional ToAddr As String() = Nothing) As ErrMailer
            Return Init(cnf, Nothing, FromAddr, ToAddr)
        End Function
        Friend Shared Function Init(cnf As ErrMailerConfig, lfo As Logfile, Optional FromAddr As String = "", Optional ToAddr As String() = Nothing) As ErrMailer
            If instance Is Nothing Then
                instance = New ErrMailer(cnf.baseConfig, lfo)
            End If
            instance.mail = instance.PrepareMail()
            If FromAddr.Length > 0 Then
                instance.DefaultFrom = FromAddr
            Else
                If cnf.FromAddr.Length > 0 Then instance.DefaultFrom = cnf.FromAddr
            End If
            If ToAddr IsNot Nothing Then
                instance.mail.PrepareHead(ToAddr)
            Else
                If cnf.ToAddr.Length > 0 Then instance.mail.PrepareHead(cnf.ToAddr)
            End If
            Return instance
        End Function

        Protected Friend Sub Add(ByRef ln As String)
            If ErrorMailMode = AutoMailMode.OnEachError Then
                SendMail()
                Dim newMail As Mailing.PreparedMail = PrepareMail()
                For Each addr As Net.Mail.MailAddress In mail.ToCollection
                    newMail.ToCollection.Add(addr)
                Next
                For Each addr As Net.Mail.MailAddress In mail.CCAddresses
                    newMail.CCAddresses.Add(addr)
                Next
                For Each addr As Net.Mail.MailAddress In mail.BBCAddresses
                    newMail.BBCAddresses.Add(addr)
                Next
                mail = newMail
            End If
            errs.Add(errs.Count + 1, ln)
        End Sub

        Public Sub Clear()
            errs.Clear()
        End Sub

        Public Sub AddError(ln As String)
            Dim line As String = Logfile.FormatLogLine(ln, "***ERROR:")
            Add(line)
        End Sub

        Public Sub AddWarning(ln As String)
            Dim line As String = Logfile.FormatLogLine(ln, "***WARNING:")
            Add(line)
        End Sub

        Public Overloads Sub SendMail(Optional ToAddr As String() = Nothing, Optional Subject As String = Nothing, Optional BodyText As String = Nothing, Optional Log As IO.FileInfo = Nothing)
            If DefaultFrom.Length = 0 Or mail.ToCollection.Count = 0 Then
                If ToAddr Is Nothing Then
                    Throw New Exception("Error Mailer - From or To not known")
                Else
                    mail.PrepareHead(ToAddr)
                End If
            End If
            If mail.Subject.Length = 0 Then
                If Subject IsNot Nothing Then
                    mail.Subject = Subject
                Else
                    mail.Subject = "On Application " & My.Application.Info.AssemblyName & " were errors"
                End If
            End If
            If mail.MailBody.Length = 0 Then
                If BodyText IsNot Nothing Then
                    mail.MailBody = BodyText
                Else
                    mail.MailBody = "On the Application " & My.Application.Info.AssemblyName & " were errors occured." & vbNewLine & vbNewLine
                End If
            End If
            For Each err As KeyValuePair(Of Integer, String) In errs
                mail.MailBody &= err.Key & vbTab & err.Value & vbNewLine
            Next
            If Log IsNot Nothing Then mail.AddAttachments({Log}.ToList())
            Try
                mail.Send()
            Catch ex As Exception
                Debug.Print(ex.Message)
                If ParentLogfile IsNot Nothing Then
                    ParentLogfile.WrLine("While error mailer tried to send mail: " & ex.Message, "***ERROR: ")
                Else
                    Try
                        Dim msg As String = "exception while error mailer whants to send mail:" & vbNewLine & vbNewLine
                        msg &= ex.Message & vbNewLine & vbNewLine
                        For Each Err_kvp As KeyValuePair(Of Integer, String) In errs
                            msg &= Err_kvp.Key & vbTab & Err_kvp.Value & vbNewLine
                        Next
                        IO.File.WriteAllText(My.Application.Info.DirectoryPath & "\errs_" & Now.ToString("yy-MM-dd") & ".txt", msg)
                    Catch : End Try
                End If
            End Try
        End Sub

        Public Sub StopMailer(Optional lf As IO.FileInfo = Nothing)
            If errs.Count > 0 And ErrorMailMode = AutoMailMode.ErrorSummary Then SendMail(Log:=lf)
        End Sub
    End Class
End Namespace