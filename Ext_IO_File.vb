﻿Imports sysio = System.IO
Namespace ext_IO
    Public Module Ext_IO_File
        ''' <summary>
        ''' Copies any files in a directory to the target directory
        ''' </summary>
        ''' <param name="CurrDir">Source directory path where the original fies are located</param>
        ''' <param name="TargetDir">Target directory path where to copy the files to</param>
        ''' <returns>Number of copied files</returns>
        Public Function CopyAnyFiles(ByVal CurrDir As String, ByVal TargetDir As String) As Integer
            Dim di As New sysio.DirectoryInfo(CurrDir)
            Dim lst As List(Of sysio.FileInfo) = CopyAnyFiles(di, TargetDir)
            Return lst.Count
        End Function
        ''' <summary>
        ''' Copies any files in a directory to the target directory
        ''' </summary>
        ''' <param name="CurrDir">Source DirectoryInfo where the original fies are located</param>
        ''' <param name="TargetDir">Target directory path where to copy the files to</param>
        ''' <returns>List of new FileInfo objects representing the copied files</returns>
        Public Function CopyAnyFiles(ByVal CurrDir As sysio.DirectoryInfo, ByVal TargetDir As String) As List(Of sysio.FileInfo)
            Dim lst As New List(Of sysio.FileInfo)
            For Each file As sysio.FileInfo In CurrDir.GetFiles()
                lst.Add(file.CopyTo(TargetDir & file.Name, True))
            Next
            Return lst
        End Function

        ''' <summary>
        ''' Moves any files located in given source directory to the target directory
        ''' </summary>
        ''' <param name="CurrDir">Source DirectoryInfo where the files are located</param>
        ''' <param name="TargetDir">Target directory path where the files have to go to</param>
        ''' <returns>Number of moved files</returns>
        Public Function MoveAnyFiles(ByVal CurrDir As sysio.DirectoryInfo, ByVal TargetDir As String) As Integer
            Dim cnt As Integer = 0
            For Each file As sysio.FileInfo In CurrDir.GetFiles()
                file.MoveTo(TargetDir & file.Name)
                cnt += 1
            Next
            Return cnt
        End Function
        ''' <summary>
        ''' Moves any files located in given source directory to the target directory
        ''' </summary>
        ''' <param name="CurrDir">Source directory path where the files are located</param>
        ''' <param name="TargetDir">Target directory path where the files have to go to</param>
        ''' <returns>Number of moved files</returns>
        Public Function MoveAnyFiles(ByVal CurrDir As String, ByVal TargetDir As String) As Integer
            Dim di As New sysio.DirectoryInfo(CurrDir)
            Return MoveAnyFiles(di, TargetDir)
        End Function

        ''' <summary>
        ''' Deletes any files which are in a given directory.
        ''' </summary>
        ''' <param name="CurrDir">DirectoryInfo where delete all files</param>
        ''' <returns>Number of deleted files</returns>
        Public Function DeleteAnyFiles(ByVal CurrDir As sysio.DirectoryInfo) As Integer
            Dim cnt As Integer = 0
            For Each file As sysio.FileInfo In CurrDir.GetFiles()
                file.Delete()
                cnt += 1
            Next
            Return cnt
        End Function
        ''' <summary>
        ''' Deletes any files which are in a given directory.
        ''' </summary>
        ''' <param name="CurrDir">Directory path where delete all files</param>
        ''' <returns>Number of deleted files</returns>
        Public Function DeleteAnyFiles(ByVal CurrDir As String) As Integer
            Dim di As New sysio.DirectoryInfo(CurrDir)
            Return DeleteAnyFiles(di)
        End Function
    End Module
End Namespace