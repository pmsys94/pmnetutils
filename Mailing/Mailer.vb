﻿Imports System.Net
Imports System.Net.Mail
Namespace Mailing
    Public Class Mailer
        Private cl As SmtpClient
        Public DefaultFrom As String
        Public ReadOnly Property NET_Mailer As SmtpClient
            Get
                Return cl
            End Get
        End Property

        Public Sub New(SMTP_Server As String, SMTP_Port As Integer, Optional UseSSL As Boolean = True,
                       Optional DefFrom As String = Nothing, Optional Insecrure_SSL_Certificates As Boolean = False)
            DefaultFrom = DefFrom
            cl = New SmtpClient(SMTP_Server, SMTP_Port)
            cl.EnableSsl = UseSSL
            If Insecrure_SSL_Certificates Then ServicePointManager.ServerCertificateValidationCallback = AddressOf AcceptAllCertifications
        End Sub

        Public Sub New(SMTP_Server As String, SMTP_Port As Integer, LogUser As String, LogPass As String, Optional UseSSL As Boolean = True,
                       Optional DefFrom As String = Nothing, Optional Insecrure_SSL_Certificates As Boolean = False)
            Me.New(SMTP_Server, SMTP_Port, UseSSL, DefFrom, Insecrure_SSL_Certificates)
            Dim cred As New NetworkCredential(LogUser, LogPass)
            cl.Credentials = cred
        End Sub

        Public Sub New(conf As MailerConfig)
            Me.New(conf.SMTP_Server, conf.SMTP_Port, conf.UseSSL, conf.DefFrom, conf.InsecureSSL)
            If conf.ConfigID = 1 Then
                Dim cred_conf As MailerConfig_wCred = CType(conf, MailerConfig_wCred)
                cl.Credentials = New NetworkCredential(cred_conf.LogUser, cred_conf.LogPass)
            End If
        End Sub
        Private Function AcceptAllCertifications(ByVal sender As Object, ByVal certification As System.Security.Cryptography.X509Certificates.X509Certificate, ByVal chain As System.Security.Cryptography.X509Certificates.X509Chain, ByVal sslPolicyErrors As System.Net.Security.SslPolicyErrors) As Boolean
            Return True
        End Function

        Public Function PrepareMail(FromAddr As String, ToAddr As List(Of String)) As PreparedMail
            Dim mail As New PreparedMail(Me)
            If Not FromAddr.Length = 0 Then
                mail.From = FromAddr
            ElseIf DefaultFrom IsNot Nothing Then
                mail.From = DefaultFrom
            End If
            If Not ToAddr.Count = 0 Then
                For Each addr As String In ToAddr
                    mail.MailObject.To.Add(addr)
                Next
            End If
            mail.CanBeSent = mail.From.Length > 0 And mail.ToCollection.Count > 0
            Return mail
        End Function
        Public Function PrepareMail(FromAddr As String, ToAddr As String()) As PreparedMail
            Return PrepareMail(FromAddr, ToAddr.ToList())
        End Function
        Public Function PrepareMail(FromAddr As String, ToAddr As String) As PreparedMail
            Dim ToList As New List(Of String)
            If ToAddr.Length > 0 Then ToList.Add(ToAddr)
            Return PrepareMail(FromAddr, ToList)
        End Function
        Public Function PrepareMail() As PreparedMail
            Return PrepareMail("", "")
        End Function

        Public Sub SendMail(ToAddr As List(Of String), CC As List(Of String), BCC As List(Of String),
                            subject As String, Optional FromAddr As String = "", Optional body As String = "", Optional Attachs As List(Of IO.FileInfo) = Nothing)
            Dim msg As PreparedMail = PrepareMail()
            msg.PrepareHead(ToAddr, CC, BCC, FromAddr)
            If body.Length > 0 Then msg.MailBody = body
            If Attachs IsNot Nothing Then msg.AddAttachments(Attachs)
            cl.Send(msg.MailObject)
        End Sub
        Public Sub SendMail(ToAddr As String(), CC As String(), BCC As String(),
                            subject As String, Optional FromAddr As String = "", Optional body As String = "", Optional Attachs As List(Of IO.FileInfo) = Nothing)
            SendMail(ToAddr.ToList, CC.ToList, BCC.ToList, subject, FromAddr, body, Attachs)
        End Sub
        Public Sub SendMail(ToAddr As String, CC As String, BCC As String,
                            subject As String, Optional FromAddr As String = "", Optional body As String = "", Optional Attachs As List(Of IO.FileInfo) = Nothing)
            SendMail({ToAddr}, {CC}, {BCC}, subject, FromAddr, body, Attachs)
        End Sub
        Public Sub SendMail(ToAddr As String(), CC As String(),
                            subject As String, Optional FromAddr As String = "", Optional body As String = "", Optional Attachs As List(Of IO.FileInfo) = Nothing)
            SendMail(ToAddr, CC, {}, subject, FromAddr, body, Attachs)
        End Sub
        Public Sub SendMail(ToAddr As String, CC As String,
                            subject As String, Optional FromAddr As String = "", Optional body As String = "", Optional Attachs As List(Of IO.FileInfo) = Nothing)
            SendMail({ToAddr}, {CC}, {}, subject, FromAddr, body, Attachs)
        End Sub
        Public Sub SendMail(ToAddr As String(),
                            subject As String, Optional FromAddr As String = "", Optional body As String = "", Optional Attachs As List(Of IO.FileInfo) = Nothing)
            SendMail(ToAddr, {}, subject, FromAddr, body, Attachs)
        End Sub
        Public Sub SendMail(ToAddr As String,
                            subject As String, Optional FromAddr As String = "", Optional body As String = "", Optional Attachs As List(Of IO.FileInfo) = Nothing)
            SendMail({ToAddr}, subject, FromAddr, body, Attachs)
        End Sub
        Public Sub SendMail(ToAddr As List(Of String), CC As List(Of String),
                            subject As String, Optional FromAddr As String = "", Optional body As String = "", Optional Attachs As List(Of IO.FileInfo) = Nothing)
            SendMail(ToAddr, CC, New List(Of String), subject, FromAddr, body, Attachs)
        End Sub
        Public Sub SendMail(ToAddr As List(Of String),
                            subject As String, Optional FromAddr As String = "", Optional body As String = "", Optional Attachs As List(Of IO.FileInfo) = Nothing)
            SendMail(ToAddr, New List(Of String), subject, FromAddr, body, Attachs)
        End Sub
    End Class
End Namespace