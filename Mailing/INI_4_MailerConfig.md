# Using a INI file for Mailer as MailerConfig

Add the following Section Schema to your INI file and including the Key Names.
All Keys are Optional. The lib will search for section and the keys as shown.

### Eample Config
SectionName: MailerConfig
SMTP_Server=smtp.mail.example
SMTP_Port=25
UseSSL=1
InsecureSSL=0
DefFrom=service@company.com
LogUser=service
LogPass=MayNotAddPlainPassword-AddByCode


### Extra Fields for Error Mailer
EFrom=errors@company.com
ETo=appdeveloper@company.com