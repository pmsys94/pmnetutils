﻿Imports System.Net
Imports System.Net.Mail
Namespace Mailing
    Public Class PreparedMail
        Protected Friend CanBeSent As Boolean
        Private UsedMailer As Mailer
        Private __mail__ As MailMessage

        Protected Friend Sub New(parent As Mailer)
            UsedMailer = parent
            __mail__ = New MailMessage()
            CanBeSent = False
        End Sub

        Public ReadOnly Property MailObject As MailMessage
            Get
                Return __mail__
            End Get
        End Property
        Public Property From As String
            Get
                If __mail__.From IsNot Nothing Then
                    Return __mail__.From.Address
                Else
                    Return ""
                End If
            End Get
            Set(value As String)
                __mail__.From = New MailAddress(value)
            End Set
        End Property

        Public ReadOnly Property ToCollection As MailAddressCollection
            Get
                Return __mail__.To
            End Get
        End Property
        Public ReadOnly Property CCAddresses As MailAddressCollection
            Get
                Return __mail__.CC
            End Get
        End Property
        Public ReadOnly Property BBCAddresses As MailAddressCollection
            Get
                Return __mail__.Bcc
            End Get
        End Property
        Public Property MailBody As String
            Get
                Return __mail__.Body
            End Get
            Set(value As String)
                __mail__.Body = value
            End Set
        End Property

        Public Property Subject As String
            Get
                Return __mail__.Subject
            End Get
            Set(value As String)
                __mail__.Subject = value
            End Set
        End Property

        Public Sub PrepareHead(ToAddr As List(Of String), CC As List(Of String), BCC As List(Of String), Optional FromAddr As String = "")
            If FromAddr.Length > 0 Then
                From = FromAddr
            ElseIf UsedMailer.DefaultFrom IsNot Nothing Then
                From = UsedMailer.DefaultFrom
            Else
                Throw New Exception("No from address given")
            End If
            If ToAddr.Count = 0 Then Throw New Exception("No TO addresses")
            For Each add As String In ToAddr
                If Not add.Contains(";,") Then
                    ToCollection.Add(add)
                Else
                    For Each subaddr As String In add.Split(";,".ToCharArray())
                        ToAddr.Add(subaddr)
                    Next
                End If
            Next
            CanBeSent = True
            For Each addr As String In CC
                If Not addr.Contains(";,") Then
                    CCAddresses.Add(addr)
                Else
                    For Each subaddr As String In addr.Split(";,".ToCharArray())
                        CCAddresses.Add(subaddr)
                    Next
                End If
            Next
            For Each addr As String In BCC
                If Not addr.Contains(";,") Then
                    BBCAddresses.Add(addr)
                Else
                    For Each subaddr As String In addr.Split(";,".ToCharArray())
                        BBCAddresses.Add(subaddr)
                    Next
                End If
            Next
        End Sub
        Public Sub PrepareHead(ToAddr As List(Of String), CC As List(Of String), Optional FromAddr As String = "")
            PrepareHead(ToAddr, CC, New List(Of String), FromAddr)
        End Sub
        Public Sub PrepareHead(ToAddr As List(Of String), Optional FromAddr As String = "")
            PrepareHead(ToAddr, New List(Of String), FromAddr)
        End Sub
        Public Sub PrepareHead(ToAddr As String(), CC As String(), BCC As String(), Optional FromAddr As String = "")
            PrepareHead(ToAddr.ToList(), CC.ToList(), BCC.ToList(), FromAddr)
        End Sub
        Public Sub PrepareHead(ToAddr As String(), CC As String(), Optional FromAddr As String = "")
            PrepareHead(ToAddr, CC, {}, FromAddr)
        End Sub
        Public Sub PrepareHead(ToAddr As String(), Optional FromAddr As String = "")
            PrepareHead(ToAddr, {}, FromAddr)
        End Sub
        Public Sub PrepareHead(ToAddr As String, CC As String, BCC As String, Optional FromAddr As String = "")
            PrepareHead({ToAddr}, {CC}, {BCC}, FromAddr)
        End Sub
        Public Sub PrepareHead(ToAddr As String, CC As String, Optional FromAddr As String = "")
            PrepareHead({ToAddr}, {CC}, FromAddr)
        End Sub
        Public Sub PrepareHead(ToAddr As String, Optional FromAddr As String = "")
            PrepareHead({ToAddr}, FromAddr)
        End Sub

        Public Sub AddAttachments(files As List(Of IO.FileInfo))
            Dim ConvList As New List(Of String)
            For Each file In files
                ConvList.Add(file.FullName)
            Next
            AddAttachments(ConvList)
        End Sub
        Public Sub AddAttachments(files As List(Of String))
            For Each file As String In files
                __mail__.Attachments.Add(New Attachment(file))
            Next
        End Sub
        Public Sub AddAttachments(files As String())
            AddAttachments(files.ToList())
        End Sub
        Public Sub AddAttachments(file As String)
            AddAttachments({file})
        End Sub

        Public Sub Send()
            If Not CanBeSent Then Return
            UsedMailer.NET_Mailer.Send(__mail__)
        End Sub
    End Class
End Namespace