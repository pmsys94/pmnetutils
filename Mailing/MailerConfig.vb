﻿Imports pmNETutils.Configurations.INI
Namespace Mailing
    ''' <summary>
    ''' Configuration Object to transport Mailer Class Settings in one object. Use this object definition to set the values from JSON / XML decoder.
    ''' Use the class functions to load the object using given sources.
    ''' Loading sources can be mixed, but check that you don't overwrite values that you already set.
    ''' After done give this object to the constructor of the (Error)Mailer class.
    ''' When the mailer is initialized you can throw the object away.
    ''' If loaded from class function you can put it into the constructors parameter list like:
    ''' <code>Dim mailer as new pmNETutils.Mailing.Mailer(new pmNETutils.Mailing.MailerConfig.FromINISection(SEC))</code>
    ''' </summary>
    Public Class MailerConfig
        Protected Friend ID As Integer = 0
        Public ReadOnly Property ConfigID As Integer
            Get
                Return ID
            End Get
        End Property
        Public SMTP_Server As String
        Public SMTP_Port As Integer
        Public UseSSL As Boolean
        Public InsecureSSL As Boolean
        Public DefFrom As String

        ''' <summary>
        ''' Load given INI file and use the given section to load configuration from there.
        ''' The file and the section must exist or you will get an exception.
        ''' It is required that the section contains the known key names as described in the Markdown doc in the git repo.
        ''' </summary>
        ''' <param name="FullPath">Full path to the INI file</param>
        ''' <param name="section">Section Name to lookup the keys.</param>
        ''' <returns>The loaded configuration object</returns>
        Public Shared Function FormINIFile_wKnownKeys(FullPath As String, section As String) As MailerConfig
            Dim conf As New MailerConfig
            conf.LoadFromINIFile_wKnownKeys(FullPath, section)
            Return conf
        End Function

        ''' <summary>
        ''' Load the configuration from the given INI Section. The section must use the known key names as shown in the Markdown doc in the git repo.
        ''' </summary>
        ''' <param name="section">Loaded INI section object with the known keys and the name <c>MailerConfig</c></param>
        ''' <returns>The loaded configuration object</returns>
        Public Shared Function FromINISection(section As INI_Section) As MailerConfig
            Dim conf As New MailerConfig
            conf.LoadFromINISection(section)
            Return conf
        End Function

        ''' <summary>
        ''' Load given INI file and use the given section to load configuration from there.
        ''' The file and the section must exist or you will get an exception.
        ''' It is required that the section contains the known key names as described in the Markdown doc in the git repo.
        ''' </summary>
        ''' <param name="FullPath">Full path to the INI file</param>
        ''' <param name="section">Section Name to lookup the keys.</param>
        Public Sub LoadFromINIFile_wKnownKeys(FullPath As String, section As String)
            Dim ini As INI_File = INI_File.OpenINIfile(FullPath)
            If Not ini.Exists Then Throw New Exception("Given INI file full path does not exist. Unable to load mailer config")
            If Not ini.HasSection(section) Then Throw New Exception("Given INI section does not exist. Unable to load mailer config from file.")
            LoadFromINISection(ini.GetSection(section))
        End Sub

        ''' <summary>
        ''' Load the configuration from the given INI Section. The section must use the known key names as shown in the Markdown doc in the git repo.
        ''' </summary>
        ''' <param name="section">Loaded INI section object with the known keys and the name <c>MailerConfig</c></param>
        Public Overridable Sub LoadFromINISection(section As INI_Section)
            If Not section.Name = "MailerConfig" Then
                Throw New Exception("Wrong section (name) in use to get MailerConfig")
            End If
            If section.HasKey("SMTP_Server") Then SMTP_Server = section.Value("SMTP_Server")
            If section.HasKey("SMTP_Port") Then SMTP_Port = CInt(section.Value("SMTP_Port"))
            If section.HasKey("UseSSL") Then UseSSL = section.Value("UseSSL") = "1"
            If section.HasKey("InsecureSSL") Then InsecureSSL = section.Value("InsecureSSL") = "1"
            If section.HasKey("DefFrom") Then DefFrom = section.Value("DefFrom")
        End Sub
    End Class

    ''' <inheritdoc/>
    Public Class MailerConfig_wCred
        Inherits MailerConfig
        Public Shadows ReadOnly Property ConfigID As Integer
            Get
                Return ID
            End Get
        End Property
        Public LogUser As String, LogPass As String

        Sub New()
            MyBase.New()
            ID = 1
        End Sub

        ''' <summary>
        ''' Load the configuration from the given INI Section. The section must use the known key names as shown in the Markdown doc in the git repo.
        ''' </summary>
        ''' <param name="section">Loaded INI section object with the known keys and the name <c>MailerConfig</c></param>
        ''' <returns>The loaded configuration object</returns>
        Public Shared Shadows Function FromINISection(section As INI_Section) As MailerConfig_wCred
            Dim conf As New MailerConfig_wCred
            conf.LoadFromINISection(section)
            Return conf
        End Function

        ''' <summary>
        ''' Load given INI file and use the given section to load configuration from there.
        ''' The file and the section must exist or you will get an exception.
        ''' It is required that the section contains the known key names as described in the Markdown doc in the git repo.
        ''' </summary>
        ''' <param name="FullPath">Full path to the INI file</param>
        ''' <param name="section">Section Name to lookup the keys.</param>
        ''' <returns>The loaded configuration object</returns>
        Public Shared Shadows Function FormINIFile_wKnownKeys(FullPath As String, section As String) As MailerConfig_wCred
            Dim conf As New MailerConfig_wCred
            conf.LoadFromINIFile_wKnownKeys(FullPath, section)
            Return conf
        End Function

        Public Overrides Sub LoadFromINISection(section As INI_Section)
            MyBase.LoadFromINISection(section)
            If section.HasKey("LogUser") Then LogUser = section.Value("LogUser")
            If section.HasKey("LogPass") Then LogPass = section.Value("LogPass")
        End Sub
    End Class
End Namespace