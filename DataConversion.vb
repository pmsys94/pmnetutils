﻿Namespace DataConversion
    Public Module DataConversion
        Public Function StringOrEmpty(s As Object) As String
            If IsNothing(s) Then Return ""
            Dim str As String = CStr(s)
            If IsDBNull(s) Then Return ""
            Return str
        End Function

        Public Function IntOrZero(i As Object) As Integer
            If IsNothing(i) Then Return 0
            If IsDBNull(i) Then Return 0
            Dim x As Integer
            If TypeOf i Is String Then
                x = Integer.Parse(CStr(i))
            ElseIf TypeOf i Is Integer Then
                x = CInt(i)
            Else
                x = 0
            End If
            Return x
        End Function

        Public Function UIntOrZero(i As Object) As UInteger
            If IsNothing(i) Then Return 0
            If IsDBNull(i) Then Return 0
            Dim x As UInteger
            If TypeOf i Is String Then
                x = UInteger.Parse(CStr(i))
            ElseIf TypeOf i Is UInteger Then
                x = CUInt(i)
            Else
                x = 0
            End If
            Return x
        End Function

        Public Function DateOrUnixDate(d As Object) As Date
            Dim dt As New Date(1970, 1, 1, 0, 0, 0)
            If IsNothing(d) Then Return dt
            If IsDBNull(d) Then Return dt
            If TypeOf d Is String Then
                dt = Date.Parse(CStr(d))
            ElseIf TypeOf d Is Date Then
                dt = CDate(d)
            End If
            Return dt
        End Function
    End Module
End Namespace