﻿Namespace Configurations
    Namespace INI
        Public Module INI_files
            Const Max_Entry = 1024

            Private Declare Ansi Function WritePrivateProfileString Lib "kernel32.dll" Alias "WritePrivateProfileStringA" (
            ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Int32

            Private Declare Ansi Function GetPrivateProfileString Lib "kernel32.dll" Alias "GetPrivateProfileStringA" (
            ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String,
            ByVal lpReturnedString As String, ByVal nSize As Int32, ByVal lpFileName As String) As Int32

            Private Declare Ansi Function GetPrivateProfileNames Lib "kernel32.dll" Alias "GetPrivateProfileStringA" (
            ByVal lpApplicationName As String, ByVal lpKeyName As Integer, ByVal lpDefault As String,
            ByVal lpszReturnBuffer() As Byte, ByVal nSize As Integer, ByVal lpFileName As String) As Integer

            Private Declare Ansi Function GetPrivateProfileSectionNames Lib "kernel32" Alias "GetPrivateProfileSectionNamesA" (
            ByVal lpszReturnBuffer() As Byte, ByVal nSize As Integer, ByVal lpFileName As String) As Integer

            ''' <summary>
            ''' Writes an entry to the given INI file into the named section. 
            ''' If either the section or the not exist in the file, they are appended to the content.
            ''' If the target file does not exist, it is also created.
            ''' </summary>
            ''' <param name="strSection">Name of the section where to set the value.</param>
            ''' <param name="strKey">Name of the key</param>
            ''' <param name="strValue">Value to store using given section and key name.</param>
            ''' <param name="strFile">Full file name of the INI file</param>
            ''' <returns>True on success, False on error</returns>
            Public Function WriteValueToFile(ByVal strSection As String, ByVal strKey As String, ByVal strValue As String, ByVal strFile As String) As Boolean
                Return Not WritePrivateProfileString(strSection, strKey, strValue, strFile) = 0
            End Function

            ''' <summary>
            ''' Reads a value from the given file name using the section and key names. If the file, section or key were not found, it return a default string.
            ''' </summary>
            ''' <param name="strSection">Section name where the key will be searched</param>
            ''' <param name="strKey">Name of the key for the value</param>
            ''' <param name="strDefault">Default string, that will be the result, if the file, section or key were not found.</param>
            ''' <param name="strFile">Full file name of the INI file</param>
            ''' <returns>Value of the section / key OR the given default string.</returns>
            Public Function ReadValueFromFile(ByVal strSection As String, ByVal strKey As String, ByVal strDefault As String, ByVal strFile As String) As String
                Dim strTemp As String = Space(Max_Entry), lLength As Integer
                If strDefault.Length > Max_Entry Then Throw New Exception("The length of the default value is to big. It must fit in the maximum of " & Max_Entry)
                lLength = GetPrivateProfileString(strSection, strKey, strDefault, strTemp, strTemp.Length, strFile)
                Return strTemp.Substring(0, lLength)
            End Function

            ''' <summary>
            ''' Returns a array of strings of all sections of the given INI file.
            ''' </summary>
            ''' <param name="strIniFile">Full INI file name</param>
            ''' <returns>Array of strings of section names OR Nothing on error.</returns>
            Public Function GetSectionNames(ByVal strIniFile As String) As String()
                Try
                    Dim buffer(Max_Entry) As Byte
                    GetPrivateProfileSectionNames(buffer, Max_Entry, strIniFile)
                    Dim parts() As String = Text.Encoding.ASCII.GetString(buffer).Trim(ControlChars.NullChar).Split(ControlChars.NullChar)
                    Return parts
                Catch : End Try
                Return Nothing
            End Function

            ''' <summary>
            ''' Returns a array of all key on a given section of a given INI file.
            ''' </summary>
            ''' <param name="section">Name of a section</param>
            ''' <param name="strIniFile">Full name of the INI file</param>
            ''' <returns>Array of strings of the keys in the given section OR Nothing on error.</returns>
            Public Function GetKeyNames(ByVal section As String, ByVal strIniFile As String) As String()
                Try
                    Dim buffer(Max_Entry) As Byte
                    GetPrivateProfileNames(section, 0, "#empty#", buffer, Max_Entry, strIniFile)
                    Dim parts() As String = Text.Encoding.ASCII.GetString(buffer).Trim(ControlChars.NullChar).Split(ControlChars.NullChar)
                    Return parts
                Catch : End Try
                Return Nothing
            End Function
        End Module
    End Namespace
End Namespace