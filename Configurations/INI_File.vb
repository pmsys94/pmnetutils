﻿Namespace Configurations
    Namespace INI
        Public Class INI_Section
            Private __parent__ As INI_File
            Protected Friend KVP As New Dictionary(Of String, String)
            Private __name__ As String
            Public ReadOnly Property Name As String
                Get
                    Return __name__
                End Get
            End Property
            Public ReadOnly Property File As INI_File
                Get
                    Return __parent__
                End Get
            End Property

            ''' <summary>
            ''' New INI Section Object
            ''' </summary>
            ''' <param name="pf">Parent INI File Object</param>
            ''' <param name="n">Name of Section</param>
            Protected Friend Sub New(pf As INI_File, n As String)
                __parent__ = pf
                __name__ = n
            End Sub

            Public Function HasKey(key As String) As Boolean
                Return KVP.ContainsKey(key)
            End Function

            Public Property Value(key As String) As String
                Get
                    Return KVP(key)
                End Get
                Set(value As String)
                    If WriteValueToFile(__name__, key, value, __parent__.fn) Then
                        If Not KVP.ContainsKey(key) Then
                            KVP.Add(key, value)
                        Else
                            KVP(key) = value
                        End If
                    Else
                        Throw New Exception("Failed to set value to corresponding INI file.")
                    End If
                End Set
            End Property

            Protected Friend Sub LoadKVP()
                Dim keys As String() = GetKeyNames(__name__, __parent__.fn)
                Dim value As String
                For Each key As String In keys
                    value = ReadValueFromFile(__name__, key, __parent__.GetDefaultValue, __parent__.fn)
                    KVP.Add(key, value)
                Next
            End Sub

            Public ReadOnly Property Keys() As String()
                Get
                    Dim ary(KVP.Count - 1) As String ' dimensions of array in vb.net are index of last element
                    For i As Integer = 0 To KVP.Keys.Count - 1
                        ary(i) = KVP.Keys.ElementAt(i)
                    Next
                    Return ary
                End Get
            End Property
        End Class



        Public Class INI_File
            Protected Friend secs As New List(Of INI_Section)
            Protected Friend fn As String = Nothing
            Public GetDefaultValue As String = Nothing
            Public ReadOnly Property Sections As String()
                Get
                    Dim ary(secs.Count - 1) As String
                    For i As Integer = 0 To secs.Count - 1
                        Dim sec As INI_Section = secs.Item(i)
                        ary(i) = sec.Name
                    Next
                    Return ary
                End Get
            End Property

            Public ReadOnly Property FilePath As String
                Get
                    Return fn
                End Get
            End Property

            Public Function GetKeys(SecName As String) As String()
                Dim sec As INI_Section = GetSection(SecName)
                Return sec.Keys
            End Function

            Public ReadOnly Property Exists As Boolean
                Get
                    Return IO.File.Exists(fn)
                End Get
            End Property

            ''' <summary>
            ''' Construct a INI file object which has no sections and keys.
            ''' This is the method when create a new file.
            ''' </summary>
            ''' <param name="FileName">Full file name of the INI file.</param>
            ''' <param name="DefaultWhenGet">String for the INI API to return on get access if the file, section or key were not found. 
            ''' If not set it is empty sting.</param>
            Public Sub New(FileName As String, Optional DefaultWhenGet As String = "")
                fn = FileName
                GetDefaultValue = DefaultWhenGet
            End Sub

            ''' <summary>
            ''' Open a existing INI file and load the sections and thier keys.
            ''' </summary>
            ''' <param name="Filename">Full file name of the INI file where it is located in filesystem.</param>
            ''' <param name="DefaultWhenGet">Default value to use if the file, sections or keys were not found on get.
            ''' If not set it is set to empty string.</param>
            ''' <returns>Returns INI File object with loaded sections and keys OR Nothing on exception.</returns>
            Public Shared Function OpenINIfile(Filename As String, Optional DefaultWhenGet As String = "") As INI_File
                Dim newFile As New INI_File(Filename, DefaultWhenGet)
                Dim newSec As INI_Section
                If Not newFile.Exists Then Return Nothing
                Dim secs As String() = GetSectionNames(Filename)
                If secs.Length = 0 Then
                    Return Nothing
                Else
                    Try
                        For Each secName As String In secs
                            newSec = New INI_Section(newFile, secName)
                            newFile.secs.Add(newSec)
                            newSec.LoadKVP()
                        Next
                    Catch ex As Exception
                        Throw New Exception("On Loading sections from file a inner exception is occured.", ex)
                        Return Nothing
                    End Try
                End If
                Return newFile
            End Function

            Public Function HasSection(name As String) As Boolean
                For Each sec As INI_Section In secs
                    If sec.Name = name Then Return True
                Next
                Return False
            End Function

            Public Function GetSection(name As String) As INI_Section
                For Each sec As INI_Section In secs
                    If sec.Name = name Then Return sec
                Next
                Throw New KeyNotFoundException
            End Function

            Public Property Item(SecName As String, Key As String) As String
                Get
                    Dim sec As INI_Section = Nothing
                    Try
                        sec = GetSection(SecName)
                    Catch ex As KeyNotFoundException
                        Throw New Exception("The given section name '" & SecName & "' was not found", ex)
                    End Try
                    Return sec.KVP(Key)
                End Get
                Set(value As String)
                    Dim sec As INI_Section
                    If HasSection(SecName) Then
                        sec = GetSection(SecName)
                        sec.Value(Key) = value
                    Else
                        sec = New INI_Section(Me, SecName)
                        sec.Value(Key) = value
                        secs.Add(sec)
                    End If
                End Set
            End Property
        End Class
    End Namespace
End Namespace